 var save_method; //for save method string
    var table;
    $(document).ready(function() {
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "dt_doa/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": 0, //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    function form_doa(doa_id)
    {
      doa_id = typeof doa_id !== 'undefined' ? doa_id : null;
      save_method = 'add';
      $('#contentPage').empty();
      $('#contentPage').load('dt_doa/form/'+doa_id+'?_=' + (new Date()).getTime());
    }
	
	function backlistdoa()
    {
      $('#contentPage').empty();
      $('#contentPage').load('dt_doa/'+'?_=' + (new Date()).getTime());
      reload_table();
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }
	
    function save()
    {
      var url;
          url = "dt_doa/ajax_proses";
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form_doa').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               alert('Proses berhasil !');
			   backlistdoa();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Proses gagal dilakukan !');
            }
        });
    }

    function delete_doa(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "dt_doa/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }