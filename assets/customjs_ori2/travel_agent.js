 var save_method; //for save method string
    var table;
    $(document).ready(function() {
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "travel_agent/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    function add_travel_agent()
    {
      save_method = 'add';
      $('#contentPage').empty();
      $('#contentPage').load('travel_agent/form'+'?_=' + (new Date()).getTime());
    }
	
	function edit_travel_agent(id)
    {
      save_method = 'edit';
      $('#contentPage').empty();
      $('#contentPage').load('travel_agent/form/'+id);
    }
	
	function detail_travel_agent(id)
    {
      save_method = 'edit';
      $('#contentPage').empty();
      $('#contentPage').load('travel_agent/form_detail/'+id);
    }
	
	function backlistta()
    {
      $('#contentPage').empty();
      $('#contentPage').load('travel_agent/'+'?_=' + (new Date()).getTime());
        reload_table();
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }
	
	function save()
    {
      var url;

      url = 'travel_agent/ajax_proses';
     
      $.ajax({
            url : url,
            type: "POST",
            data: $('#form_travel_agent').serialize(),
            dataType: "JSON",

            success: function(data)
            {
              var msg = data.message;

               //if success close modal and reload ajax table

              alert('Proses berhasil !');
              $('#contentPage').empty();
              $('#contentPage').load('travel_agent/'+'?_=' + (new Date()).getTime());
              reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $('#message').html("<div class='alert alert-danger'><i class='fa fa-check-circle-o'></i> Proses gagal </div>").delay(3000).fadeOut('slow');
            }
        });

       // ajax adding data to database
          
    }
	
    function delete_travel_agent(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "travel_agent/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               //$('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }