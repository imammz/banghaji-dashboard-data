 var save_method; //for save method string

    var table;
    var tableTour;
    var tableHotel;
    var tablePesawat;

    $(document).ready(function() {
      var paketId = $('#table-pesawat').attr('rel');
      tablePesawat = $('#table-pesawat').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": false, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "Travel_package/ajax_list_pesawat/"+paketId,
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    $(document).ready(function() {

      var paketId = $('#table-hotel').attr('rel');
      tableHotel = $('#table-hotel').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": false, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "Travel_package/ajax_list_hotel/"+paketId,
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    $(document).ready(function() {
      var paketId = $('#table-tour').attr('rel');
      tableTour = $('#table-tour').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": false, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "Travel_package/ajax_list_tour/"+paketId,
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    $(document).ready(function() {
      var paketId = $('#table').attr('rel');
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": false, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "Travel_package/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });
	
    function add_travel_package()
    {
      save_method = 'add';
      $('#contentPage').empty();
      $('#contentPage').load('Travel_package/form'+'?_=' + (new Date()).getTime());
    }
	
	function edit_travel_package(id)
    {
      save_method = 'edit';
      $('#contentPage').empty();
      $('#contentPage').load('Travel_package/form/'+id);
    }
	
	function backlisttp()
    {
      $('#contentPage').empty();
      $('#contentPage').load('Travel_package/'+'?_=' + (new Date()).getTime());
        reload_table();
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
      tableTour.ajax.reload(null,false); //reload datatable ajax 
      tableHotel.ajax.reload(null,false); //reload datatable ajax 
      tablePesawat.ajax.reload(null,false); //reload datatable ajax 
    }

   
    function save()
    {
      var url;
        url = "Travel_package/ajax_proses_paket";
      

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form_paket').serialize(),
            dataType: "JSON",
            success: function(data)
            {
              $('#contentPage').empty();
              $('#contentPage').load('Travel_package/'+'?_=' + (new Date()).getTime());
              reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_travel_package(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "Travel_package/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
              $('#contentPage').empty();
              $('#contentPage').load('Travel_package/'+'?_=' + (new Date()).getTime());
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }
	
	
	// FASILITAS TOUR //
	
  function save_tour()
    {
      var url;
        url = "Travel_package/ajax_proses_tour";
      

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form-tour').serialize(),
            dataType: "JSON",
            success: function(data)
            {
              $('#contentPage').empty();
              $('#contentPage').load('Travel_package/'+'?_=' + (new Date()).getTime());
              reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }
	
	function add_fasiltas_tour(id)
    {
      save_method = 'add';
      $('#pageTour').empty();
      $('#pageTour').load('Travel_package/add/'+id);
    }
	
	function edit_fasilitas_tour(id)
    {
      save_method = 'edit';
      $('#pageTour').empty();
      $('#pageTour').load('Travel_package/form_tour/'+id);
    }

    function delete_fasilitas_tour(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "Travel_package/ajax_delete_fasilitas_tour/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }
	
	// FASILITAS HOTEL //
	
  function save_hotel()
    {
      var url;
        url = "Travel_package/ajax_proses_tour";
      

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form-hotel').serialize(),
            dataType: "JSON",
            success: function(data)
            {
              $('#contentPage').empty();
              $('#contentPage').load('Travel_package/'+'?_=' + (new Date()).getTime());
              reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

  function delete_fasilitas_hotel(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "Travel_package/ajax_delete_fasilitas_hotel/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }

	function add_fasiltas_hotel(id)
    {
      save_method = 'add';
      $('#pageHotel').empty();
      $('#pageHotel').load('Travel_package/add_hotel/'+id);
    }
	
	/*function add_fasiltas_hotel()
    {
      save_method = 'add';
      $('#pageHotel').empty();
      $('#pageHotel').load('Travel_package/form_hotel'+'?_=' + (new Date()).getTime());
    }*/
	
  function edit_fasilitas_hotel(id)
    {
      save_method = 'edit';
      $('#pageHotel').empty();
      $('#pageHotel').load('Travel_package/form_hotel/'+id);
    }

	/*function edit_fasilitas_hotel(id)
    {
      save_method = 'edit';
      $('#pageHotel').empty();
      $('#pageHotel').load('Travel_package/form_hotel/'+id);
    }*/
	
	// FASILITAS PESAWAT //
	
  function save_pesawat()
    {
      var url;
        url = "Travel_package/ajax_proses_tour";
      

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form-pesawat').serialize(),
            dataType: "JSON",
            success: function(data)
            {
              $('#contentPage').empty();
              $('#contentPage').load('Travel_package/'+'?_=' + (new Date()).getTime());
              reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

  function delete_fasilitas_pesawat(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "Travel_package/ajax_delete_fasilitas_pesawat/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }

	function add_fasiltas_pesawat(id)
    {
      save_method = 'add';
      $('#pagePesawat').empty();
      $('#pagePesawat').load('Travel_package/add_pesawat/'+id);
    }
	
	/*function add_fasiltas_pesawat()
    {
      save_method = 'add';
      $('#pagePesawat').empty();
      $('#pagePesawat').load('Travel_package/form_pesawat'+'?_=' + (new Date()).getTime());
    }*/
	
	function edit_fasilitas_pesawat(id)
    {
      save_method = 'edit';
      $('#pagePesawat').empty();
      $('#pagePesawat').load('Travel_package/form_pesawat/'+id);
    }