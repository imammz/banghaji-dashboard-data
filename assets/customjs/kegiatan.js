 var save_method; //for save method string
    var table;
    $(document).ready(function() {
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": false, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "Dt_kegiatan/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": 0, //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    function form_kegiatan(ref_kegiatan_id)
    {
      ref_kegiatan_id = typeof ref_kegiatan_id !== 'undefined' ? ref_kegiatan_id : null;
      save_method = 'add';
      $('#contentPage').empty();
      $('#contentPage').load('Dt_kegiatan/form/'+ref_kegiatan_id+'?_=' + (new Date()).getTime());
    }
  
  function backlist_kegiatan()
    {
      $('#contentPage').empty();
      $('#contentPage').load('Dt_kegiatan/'+'?_=' + (new Date()).getTime());
      reload_table();
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }
  
    function save()
    {
      var url;
          url = "Dt_kegiatan/ajax_proses";
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form_kegiatan').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               alert('Proses berhasil !');
         backlist_kegiatan();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Proses gagal dilakukan !');
            }
        });
    }

    function delete_kegiatan(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "Dt_kegiatan/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }