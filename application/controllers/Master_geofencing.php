<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_geofencing extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Geofencing_model');
        $check = new Login_model();
        $check->_checkSession();
    }

    public function modul() {
        $modul = new StdClass();
        $modul->title = 'Master Data Geofencing';
        $modul->description = 'Modul ini digunakan untuk manajemen master Data Geofencing';

        return $modul;
    }

    public function index()
    {
        $data['modul'] = $this->modul();
        $data['title'] = 'Master Geofencing';
        $data['class'] = 'Geofencing';
        $data['view'] = "master_geofencing_view";
        $data['action'] = 'index_create_edit_master_geofencing';
        $data['geofencing_data'] = $this->Geofencing_model->_loadAllMasterGeofencing();
        $this->load->view(TEMPLATE.'/nav/standard',$data);
    }

    public function process_tambah()
    {
        if($this->input->server('REQUEST_METHOD') == 'POST') {

            $data['nama_lokasi'] = $this->input->post('nama_lokasi');
            $data['latitude'] = $this->input->post('latitude');
            $data['longitude'] = $this->input->post('longitude');

            $this->Geofencing_model->saveMasterGeofencing($data);

            redirect('Master_geofencing');

        }
    }

    public function edit_geofencing($geofencing_id)
    {
        $data['modul'] = $this->modul();
        $data['title'] = 'Master Geofencing';
        $data['class'] = 'Geofencing';
        $data['view'] = "master_geofencing_view";
        $data['action'] = 'index_create_edit_master_geofencing';
        $data['geofencing_data'] = $this->Geofencing_model->_loadAllMasterGeofencing();
        $data['geofencing'] = $this->Geofencing_model->_loadMasterGeofencingById($geofencing_id);
        $this->load->view(TEMPLATE.'/nav/standard',$data);

    }

    public function process_update($geofencing_id)
    {
        $data['nama_lokasi'] = $this->input->post('nama_lokasi');
        $data['latitude'] = $this->input->post('latitude');
        $data['longitude'] = $this->input->post('longitude');

        $this->Geofencing_model->updateMasterGeofencing($geofencing_id ,$data);

        redirect('Master_geofencing');
    }

    public function delete_geofencing($geofencing_id)
    {
        $this->Geofencing_model->deleteMasterGeofencingById($geofencing_id);

        redirect('Master_geofencing');
    }
}