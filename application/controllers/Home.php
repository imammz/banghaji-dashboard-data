<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct()
    {
        parent:: __construct();
        $this->load->model('Jamaah_model');
        $check = new Login_model();
        $check->_checkSession();

    }

    public function modul()
	{
		$modul = new StdClass();
		$modul->title = 'Data Doa-doa';
		$modul->class = 'Dt_doa';
		$modul->description = 'Modul ini digunakan untuk manajemen data doa';

		return $modul;
	}
    
	public function index()
	{
        $data = array();
        $data['modul']= $this->modul();
        $data['page_title'] = "Daftar list doa";
        $data['koordinator'] = $this->_getKordinator();
        $data['title']="Home";
        $data['class']="beranda";
        $data['view'] = "home_view";
        $data['menu'] = 'index';
        $jamaah_model = new Jamaah_model();
        $data['aktifitas_jamaah'] = $jamaah_model->_loadActivity();
        $data['panic'] = $jamaah_model->_loadPanic();
        $data['feedback'] = $jamaah_model->_loadFeedback();
        $this->load->view(TEMPLATE.'/nav/standard',$data);
	}

	public function logout()
    {

        unset($_SESSION['user_session']);
        $this->session->unset_userdata('user_session');
        $this->session->sess_destroy();

        redirect('login');
    }
}
