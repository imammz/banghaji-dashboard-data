<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kordinator extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('doa_model','doa');
        $this->load->library('grocery_CRUD');

        $check = new Login_model();
        $check->_checkSession();
	}
	public function modul()
	{
		$modul = new StdClass();
		$modul->title = 'Data Group';
		$modul->class = 'Kordinator';
		$modul->description = 'Modul ini digunakan untuk manajemen data Kordinator & Group Member';

		return $modul;
	}
	

    public function index($menu = 'crud1')
    {
		$data['modul']= $this->modul();
		$data['page_title']="Kordinator Management & Group Member";
        $view = "Kordinator_view";
		$data['view'] = $view;
        $data['menu'] = $menu;
		$this->load->view(TEMPLATE.'/nav/templates',$data);
	}

	public function crud1() {
        $crud = new Grocery_CRUD();
        $crud->set_table('group_member');
        $crud->set_subject('Kordinator Management');
        $crud->set_theme('bootstrap');

        $crud->display_as('group_id','Nama Group');
        $crud->display_as('user_id','Nama Anggota');
        $crud->display_as('anggota_paket_id','Bang Haji Code');
          
           
        $crud->columns('user_id','group_id','anggota_paket_id');
        $crud->fields('user_id','kode_member','group_id','anggota_paket_id');
           //$crud->fields('NIK','password','alamat','nama_lengkap','jenis_kelamin','nomor_pasport','email','no_hp','path_images','umur');
        $crud->unset_print();
           
        $crud->set_relation('group_id', 'group', 'group_nama');
        $crud->set_relation('user_id', 'user', 'nama_lengkap');
        $crud->set_relation('anggota_paket_id', 'anggota_paket', 'bang_haji_code');
           
        $crud->unset_export();
        $crud->unset_fields();
           
        $output = $crud->render();
        $this->load->view('tausiyah_view',$output);
    }

    public function form($fasilitas_category_id = null)
    {

    }

    public function ajax_list()
    {
        $data = array();
        $data['data'] = array();
        $data['data'][] = array('b','b','a','a','a');
        echo json_encode($data);
    }
        
	public function ajax_proses()
	{
	
	}
	
	public function _getPostData(){

	}
	
	public function ajax_delete($id)
	{
      
	}

}