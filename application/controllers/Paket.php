<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket extends CI_Controller {

    function __construct()
    {
        parent:: __construct();

        $this->load->model('Paket_model');
        $this->load->model('Jamaah_model');
        $check = new Login_model();
        $check->_checkSession();
    }

    public function modul()
	{
		$modul = new StdClass();
		$modul->title = 'Data Paket Umroh';
		$modul->class = 'Dt_doa';
		$modul->description = 'Modul ini digunakan untuk Manajemen Data Paket Umroh';

		return $modul;
	}

    public function index() {
        $this->manajemen();
    }

    public function manajemen()
	{
        $data = array();
        $data['modul']= $this->modul();
		$data['title']="Paket Umroh";
		$data['class']="paket";
		$data['view'] = "paket_view";
        $data['paket'] = $this->Paket_model->_loadAllPaketUmrohByTravelAgent($this->session->userdata('user_session')['travel_agent_id']);
		$this->load->view(TEMPLATE.'/nav/standard',$data);
	}

    public function tambah_umroh()
    {
        $data = array();
        $data['modul'] = $this->modul();
        $data['title'] = "Tambah Paket Umroh";
        $data['class'] = "paket";
        $data['view'] = "paket_create";
        $data['group'] = $this->Paket_model->_loadAllGroup();
        $data['fasilitas_hotel'] = $this->Paket_model->_loadFasilitasByCategoryId(1);
        $data['fasilitas_maskapai'] = $this->Paket_model->_loadFasilitasByCategoryId(2);
        $data['fasilitas_categories'] = $this->Paket_model->_loadAllFasilitasCategory();
        $data['fasilitas_detail_categories'] = $this->Paket_model->_loadAllFasilitasDetailCategory();
        $data['syarat'] = $this->Jamaah_model->_loadSyarat();
        $data['kegiatan'] = $this->Paket_model->_loadAllKegiatan();
        $data['koordinators'] = $this->Paket_model->_loadKoordinatorByTravelAgent($this->session->userdata('user_session')['travel_agent_id']);
        $data['master_fasilitas'] = $this->Paket_model->_loadAllFasilitasByTravelAgentId($this->session->userdata('user_session')['travel_agent_id']);

        $this->load->view(TEMPLATE. '/nav/standard', $data);
	}

	public function detail_umroh($paket_id) {
        $data = array();
        $data['modul'] = $this->modul();
        $data['title'] = "Edit Paket Umroh";
        $data['view'] = "paket_detail";
        $data['paket_detail'] =  $this->Paket_model->_loadPaketUmrohById($paket_id);
        $data['fasilitas_hotel'] = $this->Paket_model->_loadFasilitasByCategoryId(1);
        $data['fasilitas_maskapai'] = $this->Paket_model->_loadFasilitasByCategoryId(2);
        $data['fasilitas_contact'] = $this->Paket_model->_loadContactByPaket($paket_id);
        $data['list_jamaah'] = $this->Paket_model->_loadAllJamaahByPaket($paket_id);
        $data['list_review'] = $this->Paket_model->_loadAllReviewByPaket($paket_id);
        $data['list_jadwal_kegiatan'] = $this->Paket_model->_loadAllJadwalByPaket($paket_id);
        $data['list_syarat'] = $this->Paket_model->_loadAllSyaratByPaket($paket_id);
        $this->load->view(TEMPLATE. '/nav/standard', $data);
    }

    public function process_tambah_paket()
    {
        $data['paket_name'] = $this->input->post('paket_name', true);
        $data['max_kuota'] = $this->input->post('max_kuota', true);
        $data['min_kuota'] = $this->input->post('min_kuota', true);
        $data['tanggal_berangkat'] = $this->input->post('tanggal_berangkat', true);
        $data['tanggal_pulang'] = $this->input->post('tanggal_pulang', true);
        $data['keterangan'] = $this->input->post('keterangan', true);
        $data['currency'] = $this->input->post('currency');
        $data['harga_total_paket'] = $this->input->post('harga_total_paket', true);
        $data['discount'] = $this->input->post('discount');
        $data['travel_agent_id'] = $this->input->post('travel_agent_id');
        $data['koordinator_id'] = $this->input->post('koordinator_id');
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['umroh_id'] = 1;
        $data['created_by'] = $this->session->userdata('user_session')['nama_lengkap'];
        $data['jenis_paket'] = 'UMROH';
        $data['haji_id'] = 0;
        $this->Paket_model->savePaketUmroh($data);
        $paket_id = $this->db->insert_id();

        $fasilitas_hotel_data = $this->input->post('fasilitas_hotel_id');
        $fasilitas_maskapai_data = $this->input->post('fasilitas_maskapai_id');
        $paket_syarat_data = $this->input->post('ref_prasyarat_id');
        $jadwal_data = $this->input->post('jadwal');

        foreach($fasilitas_hotel_data as $row_hotel) {
            if($row_hotel != '') {
                $fasilitas_hotel['fasilitas_id'] = $row_hotel;
                $fasilitas_hotel['paket_id'] = $paket_id;
                
                $this->Paket_model->Insert('paket_fasilitas', $fasilitas_hotel);

            }
        }

        foreach($fasilitas_maskapai_data as $row_maskapai) {
            if ($row_maskapai != '') {
                $fasilitas_maskapai['fasilitas_id'] = $row_maskapai;
                $fasilitas_maskapai['paket_id'] = $paket_id;

                $this->Paket_model->Insert('paket_fasilitas', $fasilitas_maskapai);

            }
        }

        foreach ($paket_syarat_data as $row_syarat) {
            if($row_syarat != '') {
                $paket_syarat['paket_id'] = $paket_id;
                $paket_syarat['ref_prasyarat_id'] = $row_syarat;

                $this->Paket_model->Insert('paket_prasyarat', $paket_syarat);

            }
        }

        foreach ($jadwal_data as $row_jadwal) {
            if($row_jadwal != '' && isset($row_jadwal['choosed'])) {
                $jadwal['paket_id'] = $paket_id;
                $jadwal['ref_kegiatan_id'] = $row_jadwal['ref_kegiatan_id'];
                $jadwal['waktu_mulai'] = $row_jadwal['waktu_mulai'];
                $jadwal['waktu_selesai'] = $row_jadwal['waktu_selesai'];
                $jadwal['keterangan_kegiatan'] = $row_jadwal['keterangan_kegiatan'];

                $this->Paket_model->Insert('jadwal_paket', $jadwal);

            }
        }

        $this->session->set_flashdata('msg', 'Data Paket telah didaftarkan.');

        redirect('paket/manajemen');
    }

    public function process_tambah_fasilitas()
    {

        $data['travel_agent_id'] = $this->input->post('travel_agent_id', true);
        $data['fasilitas_name'] = $this->input->post('fasilitas_name', true);
        $data['fasilitas_desc'] = $this->input->post('fasilitas_desc', true);
        $data['fasilitas_star'] = $this->input->post('fasilitas_star', true);
        $data['fasilitas_motto'] = $this->input->post('fasilitas_motto', true);
        $data['fasilitas_category_id'] = $this->input->post('fasilitas_category_id', true);

        $this->Paket_model->saveFasilitas($data);

        $fasilitas_id = $this->db->insert_id();

        $detail_category = $this->input->post('detail_category');

        foreach ($detail_category as $row_detail) {

            if($row_detail['id'] != null) {
                $fasilitas_detail['fasilitas_id'] = $fasilitas_id;
                $fasilitas_detail['fasilitas_detail_category_id'] = $row_detail['id'];
                $fasilitas_detail['description'] = $row_detail['description'];

                $this->Paket_model->Insert('fasilitas_detail', $fasilitas_detail);
            }
        }


        redirect('paket/manajemen');

    }

    public function process_update_paket($paket_id)
    {
        if($this->input->server('REQUEST_METHOD') == 'POST') {
            $data['paket_name'] = $this->input->post('paket_name');
            $data['tanggal_berangkat'] = $this->input->post('tanggal_berangkat');
            $data['tanggal_pulang'] = $this->input->post('tanggal_pulang');
            $data['tanggal_berangkat'] = $this->input->post('tanggal_berangkat');
            $data['harga_total_paket'] = $this->input->post('harga_total_paket');
            $data['keterangan'] = $this->input->post('keterangan');
            $data['discount'] = $this->input->post('discount');
            $data['currency'] = $this->input->post('currency');
            $data['max_kuota'] = $this->input->post('max_kuota');
            $data['min_kuota'] = $this->input->post('min_kuota');
            $data['koordinator_id'] = $this->input->post('koordinator_id');
            $data['updated_at'] = date('Y-m-d');
            $data['updated_by'] = $this->session->userdata('user_session')['nama_lengkap'];

            $this->Paket_model->_updatePaketById($paket_id, $data);

            $fasilitas_hotel_data = $this->input->post('fasilitas_hotel_id');
            $fasilitas_maskapai_data = $this->input->post('fasilitas_maskapai_id');
            $paket_syarat_data = $this->input->post('ref_prasyarat_id');
            $jadwal_data = $this->input->post('jadwal');

            foreach($fasilitas_hotel_data as $row_hotel) {
                if($row_hotel != '') {
                    // Delete
                    $this->Paket_model->Delete('paket_fasilitas', ['fasilitas_id', $row_hotel]);
                    // Retrive
                    $fasilitas_hotel['fasilitas_id'] = $row_hotel;
                    $fasilitas_hotel['paket_id'] = $paket_id;
                    // Insert Again
                    $this->Paket_model->Insert('paket_fasilitas', $fasilitas_hotel);

                }
            }

            foreach($fasilitas_maskapai_data as $row_maskapai) {
                if ($row_maskapai != '') {
                    // Delete
                    $this->Paket_model->Delete('paket_fasilitas', ['fasilitas_id', $row_maskapai]);
                    // Retrive
                    $fasilitas_maskapai['fasilitas_id'] = $row_maskapai;
                    $fasilitas_maskapai['paket_id'] = $paket_id;
                    // Insert Again
                    $this->Paket_model->Insert('paket_fasilitas', $fasilitas_maskapai);

                }
            }

            foreach ($paket_syarat_data as $row_syarat) {
                if($row_syarat != '') {

                    $this->Paket_model->Delete('paket_prasyarat', ['ref_prasyarat_id', $row_syarat]);

                    $paket_syarat['paket_id'] = $paket_id;
                    $paket_syarat['ref_prasyarat_id'] = $row_syarat;

                    $this->Paket_model->Insert('paket_prasyarat', $paket_syarat);

                }
            }

            foreach ($jadwal_data as $row_jadwal) {
                if($row_jadwal != '' && isset($row_jadwal['choosed'])) {

                    $this->Paket_model->Delete('jadwal_paket', ['ref_kegiatan_id', $row_jadwal['ref_kegiatan_id']]);

                    $jadwal['paket_id'] = $paket_id;
                    $jadwal['ref_kegiatan_id'] = $row_jadwal['ref_kegiatan_id'];
                    $jadwal['waktu_mulai'] = $row_jadwal['waktu_mulai'];
                    $jadwal['waktu_selesai'] = $row_jadwal['waktu_selesai'];
                    $jadwal['keterangan_kegiatan'] = $row_jadwal['keterangan_kegiatan'];

                    $this->Paket_model->Insert('jadwal_paket', $jadwal);

                }
            }
            redirect('paket');
        }

        else {
            redirect('home');
        }

    }

    public function edit_umroh($paket_id)
    {
        $data['modul'] = $this->modul();
        $data['title'] = "Edit Paket Umroh";
        $data['view'] = "paket_create";
        $data['paket'] = $this->Paket_model->_loadPaketUmrohById($paket_id);
        $data['group'] = $this->Paket_model->_loadAllGroup();
        $data['fasilitas_hotel'] = $this->Paket_model->_loadFasilitasByCategoryId(1);
        $data['fasilitas_maskapai'] = $this->Paket_model->_loadFasilitasByCategoryId(2);
        $data['fasilitas_categories'] = $this->Paket_model->_loadAllFasilitasCategory();
        $data['fasilitas_detail_categories'] = $this->Paket_model->_loadAllFasilitasDetailCategory();
        $data['syarat'] = $this->Jamaah_model->_loadSyarat();
        $data['kegiatan'] = $this->Paket_model->_loadAllKegiatan();
        $data['koordinators'] = $this->Paket_model->_loadKoordinatorByTravelAgent($this->session->userdata('user_session')['travel_agent_id']);
        $data['master_fasilitas'] = $this->Paket_model->_loadAllFasilitasByTravelAgentId($this->session->userdata('user_session')['travel_agent_id']);

        $this->load->view(TEMPLATE. '/nav/standard', $data);
    }

    public function delete_umroh($paket_id)
    {
        $paket = $this->Paket_model->_loadPaketUmrohById($paket_id);

        foreach($paket['fasilitas_details'] as $row_fasilitas_details) {
            $this->Paket_model->Delete('paket_fasilitas', ['fasilitas_id', $row_fasilitas_details['fasilitas_id']]);
        }

        foreach($paket['syarat_details'] as $row_syarat_detail) {
            $this->Paket_model->Delete('paket_prasyarat', ['ref_prasyarat_id', $row_syarat_detail['ref_prasyarat_id']]);
        }

        foreach($paket['kegiatan_details'] as $row_kegiatan_detail) {
            $this->Paket_model->Delete('jadwal_paket', ['ref_kegiatan_id', $row_kegiatan_detail['ref_kegiatan_id']]);
        }

        $this->Paket_model->deletePaketUmrohById($paket_id);

        redirect('paket');
    }

    public function edit_fasilitas($fasilitas_id)
    {
        $data = array();
        $data['modul'] = $this->modul();
        $data['title'] = "Edit Paket Umroh";
        $data['view'] = "paket_create";
        $data['action'] = "edit_fasilitas";
        $data['fasilitas'] = $this->Paket_model->_loadFasilitasById($fasilitas_id);
        $data['group'] = $this->Paket_model->_loadAllGroup();
        $data['fasilitas_hotel'] = $this->Paket_model->_loadFasilitasByCategoryId(1);
        $data['fasilitas_maskapai'] = $this->Paket_model->_loadFasilitasByCategoryId(2);
        $data['fasilitas_categories'] = $this->Paket_model->_loadAllFasilitasCategory();
        $data['fasilitas_detail_categories'] = $this->Paket_model->_loadAllFasilitasDetailCategory();
        $data['syarat'] = $this->Jamaah_model->_loadSyarat();
        $data['kegiatan'] = $this->Paket_model->_loadAllKegiatan();
        $data['koordinators'] = $this->Paket_model->_loadKoordinatorByTravelAgent($this->session->userdata('user_session')['travel_agent_id']);
        $data['master_fasilitas'] = $this->Paket_model->_loadAllFasilitasByTravelAgentId($this->session->userdata('user_session')['travel_agent_id']);

        $this->load->view(TEMPLATE. '/nav/standard', $data);
    }

    public function process_update_fasilitas($fasilitas_id)
    {
        if( $this->input->server('REQUEST_METHOD') == 'POST') {

            $data['travel_agent_id'] = $this->input->post('travel_agent_id');
            $data['fasilitas_name'] = $this->input->post('fasilitas_name');
            $data['fasilitas_desc'] = $this->input->post('fasilitas_desc');
            $data['fasilitas_star'] = $this->input->post('fasilitas_star');
            $data['fasilitas_motto'] = $this->input->post('fasilitas_motto');
            $data['fasilitas_category_id'] = $this->input->post('fasilitas_category_id');

            $this->Paket_model->updateFasilitas($fasilitas_id);

            $detail_category = $this->input->post('detail_category');

            foreach ($detail_category as $row_detail) {

                if($row_detail['id'] != null) {

                    $this->Paket_model->Delete('fasilitas_detail', ['fasilitas_id', $row_detail['id']]);

                    $fasilitas_detail['fasilitas_id'] = $fasilitas_id;
                    $fasilitas_detail['fasilitas_detail_category_id'] = $row_detail['id'];
                    $fasilitas_detail['description'] = $row_detail['description'];

                    $this->Paket_model->Insert('fasilitas_detail', $fasilitas_detail);
                }
            }

            redirect('paket/tambah_umroh');

        }
        redirect('login');
    }

    public function delete_fasilitas($fasilitas_id)
    {
        $where = ['fasilitas_id', $fasilitas_id];
        $table = 'fasilitas';
        $fasilitas = $this->Paket_model->_loadFasilitasById($fasilitas_id);

        foreach ($fasilitas['details'] as $row_fasilitas_detail) {
            $this->Paket_model->Delete('fasilitas_detail', ['fasilitas_id', $row_fasilitas_detail['fasilitas_id']]);
        }

        $this->Paket_model->Delete($table, $where);

        redirect('paket/tambah_umroh');
    }
        
    public function jadwal()
	{
        $data = array();
        $data['modul']= $this->modul();
        $data['title']="Jadwal Kegiatan";
        $data['class']="paket";
        $data['view'] = "jadwal_view";

        $paket_model = new Paket_model();

        $data['paket'] = $paket_model->_loadAllPaketUmrohByTravelAgent($this->session->userdata('user_session')['travel_agent_id']);
        $data['group'] = $paket_model->_loadAllGroup();

        $this->load->view(TEMPLATE.'/nav/standard',$data);
    }
        
    public function kalender()
	{
        $data = array();
        $data['modul']= $this->modul();
        $data['title']= "Kalendar Umroh";
        $data['class']= "paket";
        $data['view'] = "kalender_view";

        $this->load->view(TEMPLATE.'/nav/standard',$data);
	}

    public function get_calendar_data()
    {
        if (!isset($_GET['start']) || !isset($_GET['end'])) {
            die("Please provide a date range.");
        }
        header('Content-Type: application/json');

        $array_paket = $this->Paket_model->_loadPaketUmrohByTravelAgentBasic($this->session->userdata('user_session')['travel_agent_id']);

        $result = json_encode($array_paket);

        echo $result;
	}

}
