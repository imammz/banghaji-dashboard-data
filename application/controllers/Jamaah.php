<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jamaah extends CI_Controller {

    function __construct()
    {
        parent:: __construct();

        $this->load->model('jamaah_model');
        $this->load->library('Gcm');
        $this->output->enable_profiler(FALSE);

        $check = new Login_model();
        $check->_checkSession();
    }

    public function modul()
	{
		$modul = new StdClass();
		$modul->title = 'Data Jamaah';
		$modul->class = 'Jamaah';
		$modul->description = 'Modul ini digunakan untuk Manajemen Data Jamaah';

		return $modul;
	}

    public function index() {
            $this->daftar();
    }


    public function daftar()
    {
        $data = array();
        $data['modul']= $this->modul();
		$data['title']="Jamaah Umroh";
		$data['class']="jamaah";
		$data['view'] = "jamaah_daftar_view";
        $data['anggota_paket'] = $this->db->select('*')
                ->select('e.nama_lengkap as kordinator_nama, c.nama_lengkap as jamaah_nama')
                ->join('paket b','a.paket_id = b.paket_id')
                ->join('user c','a.user_id = c.user_id')
                ->join('group d','a.paket_id = d.paket_id')
                ->join('admin e','d.koordinator_id = e.admin_id')
                ->get('anggota_paket a')
                ->result_array();

        $this->load->view(TEMPLATE.'/nav/standard',$data);
	}

    public function tambah_calon_jamaah()
    {
        $data['modul'] = $this->modul();
        $data['title'] = "Jamaah Umroh";
        $data['class'] = "Jamaah";
        $data['view'] = "jamaah_tambah_view";
        $data['paket'] = $this->db->get('paket')->result_array();
        $this->load->view(TEMPLATE. '/nav/standard', $data);
	}

    public function daftar_proses()
    {
            $data = array();
            $data['NIK'] = $this->input->post('NIK',true);
            $data['nama_lengkap'] = $this->input->post('nama_lengkap',true);
            $data['tanggal_lahir'] = $this->input->post('tanggal_lahir',true);
            $data['jenis_kelamin'] = $this->input->post('jenis_kelamin',true);
            $data['alamat'] = $this->input->post('alamat',true);
            $data['no_hp'] = $this->input->post('no_hp',true);
            $data['nomor_pasport'] = $this->input->post('nomor_pasport',true);
            $data['email'] = $this->input->post('email',true);
            $data['passcode'] = $this->_getPassCode();

            $this->db->insert('user',$data);
             $insert_id = $this->db->insert_id();

            $data2 = array();
            $data2['bang_haji_code'] = $data['passcode'];
            $data2['paket_id'] = $this->input->post('paket_id',true);
            $data2['user_id'] = $insert_id;
            $data2['pemesanan_id'] = 1;
            $data2['status_lunas'] = 1;
            $data2['status_sosmed'] = 1;
            $data2['status_verifikasi'] = 1;
            $data2['workflow_id'] = '001';
            
            $this->db->insert('anggota_paket',$data2);
            $anggota_insert_id = $this->db->insert_id();
            
            
            $data6 = array();
            $data6['group_id'] = 1;
            $data6['user_id'] = $insert_id;
            $data6['anggota_paket_id'] = $anggota_insert_id;
            $data6['created_at'] = date('Y-m-d H:i:s');
            
            $this->db->insert('group_member',$data6);
            
            
            $data3 = array();
            $data3['workflow_id'] = $data2['workflow_id'];
            $data3['bang_haji_code'] = $data['passcode'];
            $data3['passcode'] = $data['passcode'];
            $data3['catatan'] = 'Proses Pendaftaran';
            $data3['created_at'] = date('Y-m-d H:i:s');
            $data3['created_by'] = $this->session->userdata('user_session')['nama_lengkap'];

            $this->db->insert('workflow_logs',$data3);
            
            
            $passcode = $data['passcode'];
            
            $data4 = array();
            $data4['tahapan_visa_id'] = '001';
            $data4['bang_haji_code'] = $passcode;
            $data4['status'] = 'SELESAI';
            $data4['created_at'] = date('Y-m-d H:i:s');
            $data4['created_by'] = $this->session->userdata('user_session')['nama_lengkap'];
            $this->db->insert('status_tahapan_visa',$data4);
            
            
            $data5 = array();
            $data5['tahapan_visa_id'] = '002';
            $data5['bang_haji_code'] = $passcode;
            $data5['status'] = 'PROSES';
            $data5['created_at'] = date('Y-m-d H:i:s');
            $data5['created_by'] = $this->session->userdata('user_session')['nama_lengkap'];
            $this->db->insert('status_tahapan_visa',$data5);
            
            
            redirect('jamaah/daftar');
    }

    public function jamaah_hapus($id)
    {
        $this->db->where('anggota_paket_id', $id);
        $this->db->delete('anggota_paket');

        redirect('jamaah');
    }
        
    public function _getPassCode() {
            
        $array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

        $passcode = rand(10,99).$array[rand(0,25)]. rand(100, 999).$array[rand(0,25)];

        $check = $this->db->select('passcode')->where('passcode',$passcode)->get('user')->result_array();

        if(count($check)!=0) {
           $this->_getPassCode();
        }

        return $passcode;
    }
    
    public function pengajuan_visa()
	{
        $data = array();
        $data['modul']= $this->modul();
        $data['title']="Jamaah Umroh";
		$data['class']="jamaah";
		$data['view'] = "pengajuan_visa_view";
        $jamaah_model = new Jamaah_model();
        $data['jamaah'] = $jamaah_model->_loadJamaahPengajuanVisa();
        $data['jamaah_proses'] = $jamaah_model->_loadJamaahVisaDiProses();
        $data['syarat'] = $jamaah_model->_loadSyarat();
        
		$this->load->view(TEMPLATE.'/nav/standard',$data);
	}

    public function tambah_pengajuan_visa()
    {
        $data['modul']= $this->modul();
        $data['title']="Jamaah Umroh";
        $data['class']="jamaah";
        $data['view'] = "pengajuan_visa_tambah_view";

        $jamaah_model = new Jamaah_model();
        $data['jamaah'] = $jamaah_model->_loadJamaahPengajuanVisa();
        $data['syarat'] = $jamaah_model->_loadSyarat();
        $this->load->view(TEMPLATE.'/nav/standard',$data);
    }
        
    public function pengajuan_visa_proses() 
    {
        $anggota_paket = explode('#',$this->input->post('anggota_paket_id',TRUE));
        $passcode = $anggota_paket[1];
        $anggota_paket_id = $anggota_paket[0];
      
        $data = array();
        $data['workflow_id'] = '003'; 
        $this->db->update('anggota_paket',$data,'anggota_paket_id = '.$anggota_paket_id.'');
        
        $data3 = array();
        $data3['workflow_id'] = $data['workflow_id'];
        $data3['bang_haji_code'] = $passcode;
        $data3['passcode'] = $passcode;
        $data3['catatan'] = 'Proses Pendaftaran';
        $data3['created_at'] = date('Y-m-d H:i:s');
        $data3['created_by'] = $this->session->userdata('user_session')['nama_lengkap'];
        
        $this->db->insert('workflow_logs',$data3);
        
        $data4 = array();
        $data4['tahapan_visa_id'] = '003';
        $data4['bang_haji_code'] = $passcode;
        $data4['status'] = 'PROSES';
        $data4['created_at'] = date('Y-m-d H:i:s');
        $data4['created_by'] = $this->session->userdata('user_session')['nama_lengkap'];
        $this->db->insert('status_tahapan_visa',$data4);
        
        $data5 = array();
        $data5['status'] = 'SELESAI'; 
        $data5['updated_at'] = date('Y-m-d H:i:s');
        $data5['updated_by'] = $this->session->userdata('user_session')['nama_lengkap'];
        $this->db->update('status_tahapan_visa',$data5,"tahapan_visa_id = '002' AND bang_haji_code = '".$passcode."' ");

        //SEND GCM
        $tahapan_visa_id = '002';
        $status = 'SELESAI';
        $this->sendGcmStatusVisaToUser($anggota_paket_id, $tahapan_visa_id, $status);

        redirect('jamaah/pengajuan_visa');
        
    }
    
    public function verifikasi_visa()
    {
        $data = array();
        $data['modul']= $this->modul();
        $data['title']="Jamaah Umroh";
		$data['class']="jamaah";
		$data['view'] = "verifikasi_visa_view";
        
		$jamaah_model = new Jamaah_model();
        $data['jamaah_proses'] = $jamaah_model->_loadJamaahVisaVerifikasi();
        $data['jamaah_ditolak'] = $jamaah_model->_loadJamaahVisaDiTolak();
        $data['jamaah_diterima'] = $jamaah_model->_loadJamaahVisaDiTerima();

		$this->load->view(TEMPLATE.'/nav/standard',$data);
	}


    public function group()
    {       
        $data = array();
        $data['modul']= $this->modul();
        $data['page_title']="Group Jamaah";
        $data['menu'] = 'group_data';
		$this->load->view(TEMPLATE.'/nav/templates',$data);
	}
        
    public function group_data() 
    {
        $data = array();
        $this->load->view('group_data_view',$data);
    }
        
    public function tracking() {
        $data = array();
        $data['modul']= $this->modul();
		$data['page_title']="Group Jamaah";

        $data['menu'] = 'tracking';
		$this->load->view('frame_tracking_view',$data);
    
    }
        
    public function status_proses_tolak($anggota_paket_id,$passcode)
    {
        $data = array();
        $data['workflow_id'] = '006';
        $this->db->update('anggota_paket',$data,'anggota_paket_id = '.$anggota_paket_id.'');

        $data3 = array();
        $data3['workflow_id'] = $data['workflow_id'];
        $data3['bang_haji_code'] = $passcode;
        $data3['passcode'] = $passcode;
        $data3['catatan'] = 'Proses Pendaftaran';
        $data3['created_at'] = date('Y-m-d H:i:s');
        $data3['created_by'] = $this->session->userdata('user_session')['nama_lengkap'];
        $this->db->insert('workflow_logs',$data3);

        $data4 = array();
        $data4['tahapan_visa_id'] = '004';
        $data4['bang_haji_code'] = $passcode;
        $data4['status'] = 'GAGAL';
        $data4['created_at'] = date('Y-m-d H:i:s');
        $data4['created_by'] = $this->session->userdata('user_session')['nama_lengkap'];
        $this->db->insert('status_tahapan_visa',$data4);

        $data5 = array();
        $data5['status'] = 'SELESAI';
        $data5['updated_at'] = date('Y-m-d H:i:s');
        $data5['updated_by'] = $this->session->userdata('user_session')['nama_lengkap'];
        $this->db->update('status_tahapan_visa',$data5,"tahapan_visa_id = '003' AND bang_haji_code = '".$passcode."' ");

        //SEND GCM
        $tahapan_visa_id = '004';
        $status = 'GAGAL';
        $this->sendGcmStatusVisaToUser($anggota_paket_id, $tahapan_visa_id, $status);

        redirect('jamaah/verifikasi_visa');
    }
    
        
    public function status_proses_pending($anggota_paket_id,$passcode)
    {
        $data = array();
        $data['workflow_id'] = '005';
        $this->db->update('anggota_paket',$data,'anggota_paket_id = '.$anggota_paket_id.'');

        $data3 = array();
        $data3['workflow_id'] = $data['workflow_id'];
        $data3['bang_haji_code'] = $passcode;
        $data3['passcode'] = $passcode;
        $data3['catatan'] = 'Proses Pendaftaran';
        $data3['created_at'] = date('Y-m-d H:i:s');
        $data3['created_by'] = $this->session->userdata('user_session')['nama_lengkap'];
            
        $this->db->insert('workflow_logs',$data3);

        $data5 = array();
        $data5['status'] = 'PROSES';
        $data5['updated_at'] = date('Y-m-d H:i:s');
        $data5['updated_by'] = $this->session->userdata('user_session')['nama_lengkap'];
        $this->db->update('status_tahapan_visa',$data5,"tahapan_visa_id = '003' AND bang_haji_code = '".$passcode."' ");

        //SENDGCM
        $tahapan_visa_id = '003';
        $status = 'PROSES';
        $this->sendGcmStatusVisaToUser($anggota_paket_id, $tahapan_visa_id, $status);

        redirect('jamaah/verifikasi_visa');

    }
        
    public function status_proses_terima($anggota_paket_id,$passcode)
    {
        
        $data = array();
        $data['workflow_id'] = '004';
        $this->db->update('anggota_paket',$data,'anggota_paket_id = '.$anggota_paket_id.'');

        $data3 = array();
        $data3['workflow_id'] = $data['workflow_id'];
        $data3['bang_haji_code'] = $passcode;
        $data3['passcode'] = $passcode;
        $data3['catatan'] = 'Proses Pendaftaran';
        $data3['created_at'] = date('Y-m-d H:i:s');
        $data3['created_by'] = $this->session->userdata('user_session')['nama_lengkap'];
        $this->db->insert('workflow_logs',$data3);


        $data4 = array();
        $data4['tahapan_visa_id'] = '004';
        $data4['bang_haji_code'] = $passcode;
        $data4['status'] = 'SELESAI';
        $data4['created_at'] = date('Y-m-d H:i:s');
        $data4['created_by'] = $this->session->userdata('user_session')['nama_lengkap'];
        $this->db->insert('status_tahapan_visa',$data4);
            
        $data5 = array();
        $data5['status'] = 'SELESAI';
        $data5['updated_at'] = date('Y-m-d H:i:s');
        $data5['updated_by'] = $this->session->userdata('user_session')['nama_lengkap'];
        $this->db->update('status_tahapan_visa',$data5,"tahapan_visa_id = '003' AND bang_haji_code = '".$passcode."' ");

        //SEND GCM
        $tahapan_visa_id = '004';
        $status = 'SELESAI';
        $this->sendGcmStatusVisaToUser($anggota_paket_id, $tahapan_visa_id, $status);
        redirect('jamaah/verifikasi_visa');
    }

    public function sendGcmStatusVisaToUser($anggota_paket_id, $tahapan_visa_id, $status)
    {
        //TODO MOVE QUERY TO MODEL
        //TODO CATCH NULL POINTER
        $user_paket = $this->db->from('anggota_paket ap')
            ->join('user u','ap.user_id = u.user_id', 'left')
            ->where('ap.anggota_paket_id', $anggota_paket_id)
            ->get()->row();
        $tahapan_visa =  $this->db->from('tahapan_visa')
            ->where('tahapan_visa_id',$tahapan_visa_id)
            ->get()->row();
        if(isset($user_paket->gcm)){
            $this->Gcm->addRecepient($user_paket->gcm);
            $this->Gcm->setMessage([
                'type' => 'status_visa',
                'tahapan_visa_id' => $tahapan_visa_id,
                'tahapan' => $tahapan_visa->tahapan,
                'catatan' => $tahapan_visa->catatan,
                'tahapan_order' => $tahapan_visa->tahapan_order,
                'status' => $status
            ]);
            $this->Gcm->send();
            //print_r($this->gcm->status);
            ////print_r($this->gcm->messagesStatuses);
        }
    }
}
