<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
class Group extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('doa_model','doa');
        $this->load->library('grocery_CRUD');
        $this->output->enable_profiler(FALSE);
        $check = new Login_model();
        $check->_checkSession();
  
	}
	public function modul()
	{
		$modul = new StdClass();
		$modul->title = 'Data Group';
		$modul->class = 'Group';
		$modul->description = 'Modul ini digunakan untuk manajemen data Group Management';

		return $modul;
	}
	

    public function index($menu = 'crud1')
    {
		$data['modul']= $this->modul();
		$data['page_title']="Group Management";
		
		$view = "Group_view";
		$data['view'] = $view;
        $data['menu'] = $menu;
        $this->load->view(TEMPLATE.'/nav/templates',$data);
	}
        
    public function crud1() {
        $crud = new Grocery_CRUD();
        $crud->set_table('group');
        $crud->set_subject('Group Management');
        $crud->set_theme('bootstrap');

        $crud->display_as('koordinator_id','Kordinator');
        $crud->display_as('paket_id','Paket Umroh');


        $crud->columns('group_nama','koordinator_id','paket_id');
        //$crud->fields('NIK','password','alamat','nama_lengkap','jenis_kelamin','nomor_pasport','email','no_hp','path_images','umur');
        $crud->unset_print();
           
        $crud->set_relation('koordinator_id', 'admin', 'nama_lengkap');
        $crud->set_relation('paket_id', 'paket', 'paket_name');


        $crud->unset_export();
        $crud->unset_fields();

        $output = $crud->render();
        $this->load->view('tausiyah_view',$output);
    }
        
      

    public function form($fasilitas_category_id = null)
	{
	    //
	}

        
    public function ajax_list()
	{
        $data = array();
        $data['data'] = array();
        $data['data'][] = array('b','b','a','a','a');
        echo json_encode($data);
	}

	public function ajax_proses()
	{
	
	}
	
	public function _getPostData()
    {

	}
	
	public function ajax_delete($id)
	{
      
	}
	
	
	
}
