<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Geofencing extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Geofencing_model');
        $check = new Login_model();
        $check->_checkSession();
    }

    public function modul() {
        $modul = new StdClass();
        $modul->title = 'Data Geofencing';
        $modul->description = 'Modul ini digunakan untuk manajemen Data Geofencing';

        return $modul;
    }

    public function index()
    {
        $data['modul'] = $this->modul();
        $data['title'] = 'Geofencing';
        $data['class'] = 'Geofencing';
        $data['view'] = 'geofencing_view';
        $data['kategori'] = ['Rukun Umroh', 'Rukun Haji', 'Advertising'];
        $data['action'] = 'index_create_edit_geofencing';
        $data['geofencing_data'] = $this->Geofencing_model->_loadAllGeofencing();
        $data['master_geofencing'] = $this->Geofencing_model->_loadAllMasterGeofencing();
        $data['groups'] = $this->Geofencing_model->_loadAllGroupByTravelAgentId($this->session->userdata('user_session')['travel_agent_id']);
        $this->load->view(TEMPLATE.'/nav/standard',$data);

    }

    public function process_tambah()
    {

        if($this->input->server('REQUEST_METHOD') == 'POST') {

            $lokasi_lat_long = explode('|', $this->input->post('nama_lokasi'));
            $nama_lokasi = $lokasi_lat_long[0];

            $data['group_id'] = $this->input->post('group_id');
            $data['latitude'] = $this->input->post('latitude');
            $data['longitude'] = $this->input->post('longitude');
            $data['longitude'] = $this->input->post('longitude');
            $data['nama_lokasi'] = $nama_lokasi;
            $data['radius'] = $this->input->post('radius');
            $data['kategori'] = $this->input->post('kategori');

            $obj = new stdClass();
            $obj->arti = $this->input->post('arti');
            $obj->title = $this->input->post('title');
            $obj->text_arab = $this->input->post('text_arab');
            $obj->text_latin = $this->input->post('text_latin');

            $data['content'] = json_encode(array($obj));

            $this->Geofencing_model->saveGeofencing($data);

            redirect('Geofencing');

        }
    }

    public function edit_geofencing($geofencing_id)
    {
        $data['modul'] = $this->modul();
        $data['title'] = 'Geofencing';
        $data['class'] = 'Geofencing';
        $data['view'] = "geofencing_view";
        $data['action'] = 'index_create_edit_geofencing';
        $data['kategori'] = ['Rukun Umroh', 'Rukun Haji', 'Advertising'];
        $data['master_geofencing'] = $this->Geofencing_model->_loadAllMasterGeofencing();
        $data['geofencing_data'] = $this->Geofencing_model->_loadAllGeofencing();
        $data['geofencing'] = $this->Geofencing_model->_loadGeofencingById($geofencing_id);
        $data['groups'] = $this->Geofencing_model->_loadAllGroupByTravelAgentId($this->session->userdata('user_session')['travel_agent_id']);
        $content = json_decode($data['geofencing']['content']);
        foreach ($content as $row) {
            $data['arti'] = $row->arti;
            $data['title'] = $row->title;
            $data['text_arab'] = $row->text_arab;
            $data['text_latin'] = $row->text_latin;
        }

        $this->load->view(TEMPLATE.'/nav/standard',$data);

    }

    public function process_update($geofencing_id)
    {

        if($this->input->server('REQUEST_METHOD') == 'POST') {
            $lokasi_lat_long = explode('|', $this->input->post('nama_lokasi'));
            $nama_lokasi = $lokasi_lat_long[0];

            $data['group_id'] = $this->input->post('group_id');
            $data['latitude'] = $this->input->post('latitude');
            $data['longitude'] = $this->input->post('longitude');
            $data['longitude'] = $this->input->post('longitude');
            $data['nama_lokasi'] = $nama_lokasi;
            $data['radius'] = $this->input->post('radius');
            $data['kategori'] = $this->input->post('kategori');

            $obj = new stdClass();
            $obj->arti = $this->input->post('arti');
            $obj->title = $this->input->post('title');
            $obj->text_arab = $this->input->post('text_arab');
            $obj->text_latin = $this->input->post('text_latin');

            $data['content'] = json_encode(array($obj));

            $this->Geofencing_model->updateGeofencing($geofencing_id ,$data);

            redirect('Geofencing');
        }

    }

    public function delete_geofencing($geofencing_id)
    {
        $this->Geofencing_model->deleteGeofencingById($geofencing_id);

        redirect('Gofencing');
    }

}