<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dt_doa extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('doa_model','doa');
        $this->load->library('grocery_CRUD');
        $this->output->enable_profiler(FALSE);
        $check = new Login_model();
        $check->_checkSession();
	}
	public function modul()
	{
		$modul = new StdClass();
		$modul->title = 'Data Doa-doa';
		$modul->class = 'Dt_doa';
		$modul->description = 'Modul ini digunakan untuk manajemen data doa';

		return $modul;
	}
	

    public function index($menu = 'crud1')
    {
        $data['modul']= $this->modul();
        $data['page_title']="Daftar list doa";
        $view = "doa_view";
        $data['view'] = $view;
        $data['menu'] = $menu;
        $this->load->view(TEMPLATE.'/nav/templates',$data);
		
	}
	
    public function crud3() {
        $content_kategori_id = 8;

        $crud = new Grocery_CRUD();
        $crud->set_table('content_detail');
        $crud->where('content_umroh.content_kategori_id',$content_kategori_id);
        $crud->set_subject('Doa Haji');
        $crud->set_theme('bootstrap');
           
        $crud->display_as('content_detail','Arti (B.Indonesia)');
        $crud->display_as('content_detail_arabic','Lafaz Doa (Dalam Tulisan Arab)');
        $crud->display_as('content_detail_latin','Lafaz Doa (Dalam Tulisan Bahasa )');
        $crud->display_as('filename','Upload File');
        $crud->display_as('ref_filetype_id','Type File');
        $crud->display_as('url_video','URL Video');
        $crud->display_as('order','Urutan Doa');
           
        $crud->field_type('content_umroh_id', 'invisible');
        $crud->columns('order','judul','content_detail_arabic','content_detail');
        $crud->set_field_upload('filename','upload/file');

        $crud->fields('order','filename','ref_filetype_id','judul','content_detail','content_detail_arabic','content_detail_latin','url_video','content_umroh_id');
        $crud->unset_print();

        $crud->set_relation('content_umroh_id', 'content_umroh', 'judul','content_kategori_id IN ('.$content_kategori_id.')');
        $crud->set_relation('ref_filetype_id', 'ref_filetype', 'filetype','ref_filetype_id IN (1,5,6)');
        $crud->unset_export();
        $crud->unset_fields();

        $_SESSION['content_kategori_id'] = $content_kategori_id;
        $crud->callback_before_insert(array($this,'_beforeInsertDoa'));

        $output = $crud->render();
        $this->load->view('crud_view',$output);
    }
        
    public function crud1() {
	    $content_kategori_id = 9;
            
        $crud = new Grocery_CRUD();
        $crud->set_table('content_detail');
        $crud->where('content_umroh.content_kategori_id',$content_kategori_id);
        $crud->set_subject('Doa harian');
        $crud->set_theme('bootstrap');
           
        $crud->display_as('content_detail','Arti (B.Indonesia)');
        $crud->display_as('content_detail_arabic','Lafaz Doa (Dalam Tulisan Arab)');
        $crud->display_as('content_detail_latin','Lafaz Doa (Dalam Tulisan Bahasa )');
        $crud->display_as('filename','Upload File');
        $crud->display_as('ref_filetype_id','Type File');
        $crud->display_as('url_video','URL Video');
        $crud->display_as('order','Urutan Doa');
           
        $crud->field_type('content_umroh_id', 'invisible');
        $crud->columns('order','judul','content_detail_arabic','content_detail');
        $crud->set_field_upload('filename','upload/file');

        $crud->fields('order','filename','ref_filetype_id','judul','content_detail','content_detail_arabic','content_detail_latin','url_video','content_umroh_id');
        $crud->unset_print();
           
        $crud->set_relation('content_umroh_id', 'content_umroh', 'judul','content_kategori_id IN ('.$content_kategori_id.')');
        $crud->set_relation('ref_filetype_id', 'ref_filetype', 'filetype','ref_filetype_id IN (1,5,6)');

        $crud->unset_export();
        $crud->unset_fields();

        $_SESSION['content_kategori_id'] = $content_kategori_id;
        $crud->callback_before_insert(array($this,'_beforeInsertDoa'));

        $output = $crud->render();
        $this->load->view('crud_view',$output);
    }
        
        
    public function _beforeInsertDoa($post_array) {
        $content_umroh = array();
        $content_umroh['judul'] = $post_array['judul'];
        $content_umroh['content_kategori_id'] = $_SESSION['content_kategori_id'];
        $content_umroh['admin_id'] = 0;
        $this->db->insert('content_umroh',$content_umroh);
        $id = $this->db->insert_id();

        $post_array['content_umroh_id'] = $id;

        unset($_SESSION['content_kategori_id']);

        return $post_array;
    }
        
        
    public function crud2() {
        $content_kategori_id = 10;

        $crud = new Grocery_CRUD();
        $crud->set_table('content_detail');
        $crud->where('content_umroh.content_kategori_id',$content_kategori_id);
        $crud->set_subject('Doa Harian');
        $crud->set_theme('bootstrap');

        $crud->display_as('content_detail','Arti (B.Indonesia)');
        $crud->display_as('content_detail_arabic','Lafaz Doa (Dalam Tulisan Arab)');
        $crud->display_as('content_detail_latin','Lafaz Doa (Dalam Tulisan Bahasa )');
        $crud->display_as('filename','Upload File');
        $crud->display_as('ref_filetype_id','Type File');
        $crud->display_as('url_video','URL Video');
        $crud->display_as('order','Urutan Doa');

        $crud->field_type('content_umroh_id', 'invisible');
        $crud->columns('order','judul','content_detail_arabic','content_detail');
        $crud->set_field_upload('filename','upload/file');

        $crud->fields('order','filename','ref_filetype_id','judul','content_detail','content_detail_arabic','content_detail_latin','url_video','content_umroh_id');
        $crud->unset_print();
           
        $crud->set_relation('content_umroh_id', 'content_umroh', 'judul','content_kategori_id IN ('.$content_kategori_id.')');
        $crud->set_relation('ref_filetype_id', 'ref_filetype', 'filetype','ref_filetype_id IN (1,5,6)');

        $crud->unset_export();
        $crud->unset_fields();

        $_SESSION['content_kategori_id'] = $content_kategori_id;
        $crud->callback_before_insert(array($this,'_beforeInsertDoa'));

        $output = $crud->render();
        $this->load->view('crud_view',$output);
    }

    public function form($fasilitas_category_id = null)
    {
    }

        
    public function ajax_list()
    {
        $data = array();
        $data['data'] = array();
        $data['data'][] = array('b', 'b', 'a', 'a', 'a');
        echo json_encode($data);
    }

    public function ajax_proses()
	{
	
	}
	
	public function _getPostData()
    {

	}
	
	public function ajax_delete($id)
	{
      
	}

}
