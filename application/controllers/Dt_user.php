<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dt_user extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('doa_model','doa');
        $this->load->library('grocery_CRUD');
        $this->load->library('Firebase');
        $this->output->enable_profiler(FALSE);
        $check = new Login_model();
        $check->_checkSession();
        	
  
	}
	public function modul()
	{
		$modul = new StdClass();
		$modul->title = 'Data User Managemen';
		$modul->class = 'Dt_user';
		$modul->description = 'Modul ini digunakan untuk manajemen data User Management';

		return $modul;
	}
	

        public function index($menu = 'crud1')
	{   
		$data['modul']= $this->modul();
		$data['page_title']="User Management";
		
		$view = "Dt_user_view";
		$data['view'] = $view;
                $data['menu'] = $menu;
		$this->load->view(TEMPLATE.'/nav/templates',$data);
		
	}
	
        
        
        public function crud1() {
          $crud = new Grocery_CRUD();
           $crud->set_table('user');
           //$crud->where('content_umroh.content_kategori_id IN (5,6)');
           //$crud->set_theme('');
           $crud->set_subject('User Management');
           $crud->set_theme('bootstrap');
           
           $crud->display_as('NIK','Nomor KTP');
           $crud->display_as('umur','Usia');
          
           $crud->set_field_upload('path_images','upload/file');
           
           $crud->columns('email','nama_lengkap','NIK');
           $crud->fields('NIK','password','alamat','nama_lengkap','jenis_kelamin','nomor_pasport','email','no_hp','path_images','umur');
           $crud->unset_print();
           
           
           $crud->unset_export();
           $crud->unset_fields();


            $crud->callback_after_insert(array($this, 'insert_firebase'));
            $crud->callback_after_update(array($this, 'update_firebase'));
            $crud->callback_after_delete(array($this, 'delete_firebase'));

           $output = $crud->render();
           $this->load->view('tausiyah_view',$output);
        }

    function insert_firebase($post_array, $primary_key)
    {
        $user = array(
            "user_id" => $primary_key,
            "nama" => $post_array['nama_lengkap'],
            "no_hp" => $post_array['no_hp'],
            "profile_pict" => $post_array['path_images'],
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        );
        $firebase = New Firebase();
        $firebase->insertUser($user, $primary_key);
    }

    function update_firebase($post_array, $primary_key)
    {
        if(isset($post_array['nama_lengkap'])){
            $user['nama'] = $post_array['nama_lengkap'];
        }
        if(isset($post_array['no_hp'])){
            $user['no_hp'] = $post_array['no_hp'];
        }
        if(isset($post_array['path_images'])){
            $user['profile_pict'] = $post_array['path_images'];
        }
        $user['updated_at'] = date('Y-m-d H:i:s');

        $firebase = New Firebase();
        $firebase->insertUser($user, $primary_key);
    }

    function delete_firebase($primary_key)
    {
        $firebase = New Firebase();
        $firebase->deleteUser($primary_key);
    }
      

    public function form($fasilitas_category_id = null)
	{
        

    }

        
        public function ajax_list()
	{
            $data = array();
           $data['data'] = array();
           $data['data'][] = array('b','b','a','a','a');
           echo json_encode($data);
	}
        
	public function ajax_proses()
	{
	
	}
	
	public function _getPostData(){

	}
	
	public function ajax_delete($id)
	{
      
	}
	
	
	
}
