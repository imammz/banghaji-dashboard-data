<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('user_session')) {
            redirect('/');
        }
    }

    public function index(){
        $this->load->view('login_view');
    }

    public function login_post(){
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        if(!empty($email) || !empty($password)) {

            if($this->login_model->_checkPassword($email, $password)) {
                $query =  $this->login_model->_getDataAdmin($email);
                $result = $query;
                $login = array();
                $login['id'] = $result['admin_id'];
                $login['email'] = $result['email'];
                $login['nama_lengkap'] = $result['nama_lengkap'];
                $login['travel_agent_id'] = $result['travel_agent_id'];
                $login['alamat'] = $result['alamat'];
                $login['no_hp'] = $result['no_hp'];
                $login['path_images'] = $result['path_images'];
                $login['login'] = true;
                $login['nama'] = $result['travel_agent_name'];

                $this->session->set_userdata('user_session', $login);

                redirect('/');
            } else {
                $_SESSION['error'] = 'Username atau Password tidak sesuai.';

                redirect('/login');
            }
        } else {
            $_SESSION['error'] = 'Username atau password tidak boleh kosong.';

            redirect('/login');
        }
    }


}


