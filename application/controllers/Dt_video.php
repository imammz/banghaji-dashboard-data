<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
class Dt_video extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('doa_model','doa');
                $this->load->library('grocery_CRUD');
        $this->output->enable_profiler(FALSE);
        $check = new Login_model();
        $check->_checkSession();
        	
  
	}
	public function modul()
	{
		$modul = new StdClass();
		$modul->title = 'Data Berita Seputar Haji';
		$modul->class = 'Dt_video';
		$modul->description = 'Modul ini digunakan untuk manajemen data Content Video';

		return $modul;
	}
	

        public function index($menu = 'crud1')
	{   
		$data['modul']= $this->modul();
		$data['page_title']="Daftar Content Video";
		
		$view = "Dt_video_view";
		$data['view'] = $view;
                $data['menu'] = $menu;
		$this->load->view(TEMPLATE.'/nav/templates',$data);
		
	}
	
        
        
        public function crud1() {
          $crud = new Grocery_CRUD();
           $crud->set_table('content_umroh');
           $crud->where('content_umroh.content_kategori_id IN (5,6)');
           //$crud->set_theme('');
           $crud->set_subject('Content Video / Audio');
           $crud->set_theme('bootstrap');
           
           $crud->display_as('content_kategori_id','Kategori');
           $crud->display_as('content','Keterangan Content');
           $crud->display_as('author','Author');
           $crud->display_as('file_audio','Audio');
           $crud->display_as('url_video','Video');
           $crud->display_as('file_image','Images Thumbnail');
         
           $crud->set_field_upload('file_audio','upload/file');
           $crud->set_field_upload('url_video','upload/file');
           $crud->set_field_upload('file_image','upload/file');
           
           $crud->columns('judul','tanggal');
           $crud->fields('judul','content_kategori_id','content','tanggal','author','url_video','file_audio','file_image');
           $crud->unset_print();
           
           $crud->set_relation('content_kategori_id', 'content_kategori', 'kategori','content_kategori_id IN (3)');
           
           $crud->unset_export();
           $crud->unset_fields();
           
           $output = $crud->render();
           $this->load->view('tausiyah_view',$output);
        }
        
      

        public function form($fasilitas_category_id = null)
	{
        

        }

        
        public function ajax_list()
	{
            $data = array();
           $data['data'] = array();
           $data['data'][] = array('b','b','a','a','a');
           echo json_encode($data);
	}
        
	public function ajax_proses()
	{
	
	}
	
	public function _getPostData(){

	}
	
	public function ajax_delete($id)
	{
      
	}
	
	
	
}
