<?php
/**
 * Created by PhpStorm.
 * User: Neverier00
 * Date: 12/11/2016
 * Time: 7:10 PM
 */
class Firebase
{
    const DEFAULT_URL = 'https://banghaji.firebaseio.com/';
    const DEFAULT_TOKEN = 'ns4z29nA6u6KXvOx0ILWGlsaiH5hk0YkwX8DZsH2';
    const DEFAULT_PATH = '';

    private $firebase;

    function __construct()
    {
        $this->firebase = new \Firebase\FirebaseLib(self::DEFAULT_URL, self::DEFAULT_TOKEN);
    }

    public function insertUser($user, $primary_key)
    {
        //TODO GENERATE MARKER;
        $user['marker_xhdpi'] = "http://pocer.in:4006/public/files/marker/marker_xhdpi.png";
        $user['marker_xxxhdpi'] = "http://pocer.in:4006/public/files/marker/marker_xxxhdpi.png";
        $this->firebase->set(self::DEFAULT_PATH . '/users/'.$primary_key, $user);
    }

    public function updateUser($user, $primary_key)
    {
        $this->firebase->update(self::DEFAULT_PATH . '/users/'.$primary_key, $user);
    }

    public function deleteUser($user_id)
    {
        $this->firebase->delete(self::DEFAULT_PATH . '/users/', $user_id);
    }

}