<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jamaah_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function _loadActivity() {
        
        $ress = $this->db->from('user a')
                ->order_by('nama_lengkap ASC')
                ->get()->result_array();
        
        $user = array();
        
        foreach($ress as $row) {
            $row['activity'] = $this->db->where('user_id',$row['user_id'])
                ->order_by('created_at DESC')
                ->get('log_user_activity')
                ->result_array();
            $user[] = $row;
        }
        return $user;
        
    }
    
    public function _loadPanic() {
        
        $ress = $this->db
                ->select('*','b.nama_lengkap as kordinator_nama')
                ->from('group a')
                ->join('admin b','a.koordinator_id = b.admin_id','LEFT')
                ->order_by('group_id ASC')
                ->get()->result_array();
        
        $user = array();
        
        foreach($ress as $row) {
            $row['alert'] = $this->db->where('a.group_id',$row['group_id'])
                    ->select([
                        'c.group_id as group_group_id', 'c.group_nama',
                        'a.group_alert_id','a.group_id', 'a.user_id', 'b.nama_lengkap', 'b.alamat',
                        'a.created_at', 'a.lokasi', 'b.email', 'a.keterangan', 'a.latitude', 'a.longitude', 'b.no_hp', 'b.jenis_kelamin', 'a.image_path'
                    ])
                    ->from('group_alert a')
                    ->join('group c', 'c.group_id = a.group_id')
                    ->join('user b','a.user_id = b.user_id')
                    ->order_by('a.created_at DESC')
                    ->get()->result_array();
            $user[] = $row;
        }
        return $user;
    }

    public function _loadJamaahPengajuanVisa() {
        $ress = $this->db->from('anggota_paket a')
                ->join('paket b','a.paket_id = b.paket_id')
                ->join('user c','a.user_id = c.user_id')
                ->join('group_member d','a.anggota_paket_id = d.anggota_paket_id')
                ->join('group e','e.group_id = d.group_id')
                ->join('workflow f','a.workflow_id = f.workflow_id')
                ->where('b.travel_agent_id',$this->session->userdata('user_session')['travel_agent_id'])
                ->where('a.workflow_id','001')
                ->get()->result_array();
        
        return $ress;
    }
    
    public function _loadJamaahVisaDiProses() {
        $ress = $this->db->from('anggota_paket a')
                ->join('paket b','a.paket_id = b.paket_id')
                ->join('user c','a.user_id = c.user_id')
                ->join('group_member d','a.anggota_paket_id = d.anggota_paket_id')
                ->join('group e','e.group_id = d.group_id')
                ->join('workflow f','a.workflow_id = f.workflow_id')
                ->where('b.travel_agent_id',$this->session->userdata('user_session')['travel_agent_id'])
                ->where("a.workflow_id != '001'")
                ->get()->result_array();
        
        return $ress;
    }

    public function _loadJamaahVisaDiTolak() {
        $ress = $this->db->from('anggota_paket a')
            ->join('paket b','a.paket_id = b.paket_id')
            ->join('user c','a.user_id = c.user_id')
            ->join('group_member d','a.anggota_paket_id = d.anggota_paket_id')
            ->join('group e','e.group_id = d.group_id')
            ->join('workflow f','a.workflow_id = f.workflow_id')
            ->where('b.travel_agent_id',$this->session->userdata('user_session')['travel_agent_id'])
            ->where('a.workflow_id', 006)
            ->get()->result_array();

        return $ress;
    }

    public function _loadJamaahVisaDiTerima() {
        $ress = $this->db->from('anggota_paket a')
            ->join('paket b','a.paket_id = b.paket_id')
            ->join('user c','a.user_id = c.user_id')
            ->join('group_member d','a.anggota_paket_id = d.anggota_paket_id')
            ->join('group e','e.group_id = d.group_id')
            ->join('workflow f','a.workflow_id = f.workflow_id')
            ->where('b.travel_agent_id',$this->session->userdata('user_session')['travel_agent_id'])
            ->where('a.workflow_id', 004)
            ->get()->result_array();

        return $ress;
    }

    public function _loadJamaahVisaVerifikasi() {
        $ress = $this->db->from('anggota_paket a')
                ->join('paket b','a.paket_id = b.paket_id')
                ->join('user c','a.user_id = c.user_id')
                ->join('group_member d','a.anggota_paket_id = d.anggota_paket_id')
                ->join('group e','e.group_id = d.group_id')
                ->join('workflow f','a.workflow_id = f.workflow_id')
                ->where('b.travel_agent_id',$this->session->userdata('user_session')['travel_agent_id'])
                ->where("a.workflow_id IN ('002','003')")
                ->get()->result_array();
        
        return $ress;
    }

    public function _loadSyarat() {
        $ress = $this->db->from('ref_prasyarat a')
                ->get()->result_array();
        
        return $ress;
    }

    public function _loadFeedback()
    {
        $ress = $this->db->select('*')
            ->select(
                ['a.created_at as group_created_at',
                'b.nama_lengkap as nama_jamaah',
                'e.nama_lengkap as nama_koordinator'])
            ->from('group_feedback a')
            ->join('user b', 'a.user_id = b.user_id')
            ->join('group c', 'a.group_id = c.group_id')
            ->join('paket d', 'd.paket_id = c.paket_id')
            ->join('admin e', 'e.admin_id = c.koordinator_id')
            ->get()->result_array();

        return $ress;
    }
    

}