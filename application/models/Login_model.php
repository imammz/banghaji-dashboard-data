<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model{

    function __construct()
    {
        parent:: __construct();
    }

    public function _checkSession() {
        if(  $this->session->userdata('user_session')['login'] == true) {
            return true;
        } else {
            $_SESSION['error'] = 'Silahkan login terlebih dahulu';
            redirect('login');
        }
    }

    public function _checkPassword($email, $password){

        $hashedPassword = $this->getPasswordByEmail($email);

        if(password_verify($password, $hashedPassword)) {
            return true;
        } else {
            return false;
        }

    }

    function getPasswordByEmail($email) {
        $this->db->select('password');
        $this->db->from("admin");
        $this->db->where("email",$email);

        $query = $this->db->get();
        foreach ($query->result() as $row)
        {
            return $row->password;
        }
        return false;
    }

    public function _getDataAdmin($email) {
        $this->load->database();
        //$data = $this->db->query("select * from admin where email = '$email' limit 0,1")->row_array();
        $ress = $this->db->where('a.email', $email)
            ->select([
                'b.nama as travel_agent_name',
                'a.admin_id','a.travel_agent_id', 'a.email', 'a.nama_lengkap', 'a.alamat', 'a.no_hp', 'a.path_images'
            ])
            ->join('travel_agent b','a.travel_agent_id = b.travel_agent_id','LEFT')
            ->join('role c','a.role_id = c.role_id','LEFT')
            ->get('admin a')
            ->row_array();

        return $ress;
    }
}