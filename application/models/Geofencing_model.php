<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Geofencing_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function _loadAllMasterGeofencing()
    {
        $ress = $this->db->from('master_geofencing')
            ->get()->result_array();

        return $ress;
    }

    public function _loadMasterGeofencingById($master_geofencing_id)
    {
        $ress = $this->db->where('master_geofencing_id', $master_geofencing_id)
            ->from('master_geofencing')
            ->get()->row_array();

        return $ress;
    }

    public function saveMasterGeofencing($data)
    {
        $this->db->insert('master_geofencing', $data);
    }

    public function updateMasterGeofencing($id ,$data)
    {
        $this->db->where('master_geofencing_id', $id);
        $this->db->update('master_geofencing', $data);
    }

    public function deleteMasterGeofencingById($id)
    {
        $this->db->where('master_geofencing_id', $id);
        $this->db->delete('master_geofencing');
    }

    public function _loadAllGroupByTravelAgentId($travel_agent_id)
    {
        $ress = $this->db->where('travel_agent_id', $travel_agent_id)
            ->from('group')
            ->get()->result_array();

        return $ress;
    }

    public function _loadAllGeofencing()
    {
        $ress = $this->db->from('geofencing a')
            ->select(['b.group_nama', 'a.*'])
            ->join('group b', 'a.group_id = b.group_id')
            ->get()
            ->result_array();

        return $ress;
    }

    public function _loadGeofencingById($geofencing_id)
    {
        $ress = $this->db->where('geofencing_id', $geofencing_id)
            ->from('geofencing')
            ->get()
            ->row_array();

        return $ress;
    }

    public function saveGeofencing($data)
    {
        $this->db->insert('geofencing', $data);
    }

    public function updateGeofencing($id ,$data)
    {
        $this->db->where('geofencing_id', $id);
        $this->db->update('geofencing', $data);
    }

    public function deleteGeofencingById($id)
    {
        $this->db->where('geofencing_id', $id);
        $this->db->delete('geofencing');
    }
    
}