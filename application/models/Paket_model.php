<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Paket_model extends CI_Model {

    var $table = 'paket';

    public function __construct() {
        parent::__construct();
    }

    public function _loadAllPaketUmrohByTravelAgent($travel_agent_id) {
        $ress = $this->db->where('a.travel_agent_id',$travel_agent_id)
            ->select(['a.paket_id', 'a.paket_name', 'b.nama', 'a.tanggal_berangkat', 'a.tanggal_pulang', 'c.nama_lengkap', 'c.no_hp', 'a.harga_total_paket'])
            ->from('paket a')
            ->where('a.jenis_paket', 'UMROH')
            ->order_by('tanggal_berangkat ASC')
            ->join('admin c', 'c.travel_agent_id = a.travel_agent_id')
            ->join('travel_agent b', 'b.travel_agent_id = c.travel_agent_id')
            ->join('group d', 'd.koordinator_id = c.admin_id')
            ->get()->result_array();
        return $ress;
        
    }

    public function _loadPaketUmrohByTravelAgentBasic($travel_agent_id)
    {
        $ress = $this->db->where('travel_agent_id', $travel_agent_id)
                ->select([
                    'paket_id', 'paket_name as title', 'tanggal_berangkat as start', 'tanggal_pulang as end',
                    'harga_total_paket', 'currency', 'keterangan', 'max_kuota', 'min_kuota'
                ])
                ->from('paket')
                ->get()->result();

        return $ress;
    }

    public function _loadAllPaketUmroh() {
        $ress = $this->db->order_by('tanggal_berangkat ASC')
            ->where('jenis_paket', 'UMROH')
            ->join('group c','c.paket_id = a.paket_id')
            ->join('admin b','b.travel_agent_id = d.travel_agent_id')
            ->join('travel_agent d', 'a.travel_agent_id = d.travel_agent_id')
            ->get('paket a')->result_array();

        return $ress;
    }

    public function _loadPaketUmrohById($id)
    {
        $ress = $this->db->select([
                'a.paket_id', 'a.paket_name', 'a.max_kuota', 'a.min_kuota', 'a.tanggal_berangkat', 'a.tanggal_pulang',
                'a.keterangan', 'a.harga_total_paket', 'a.discount', 'a.currency', 'a.travel_agent_id', 'a.koordinator_id'
            ])
            ->order_by('tanggal_berangkat ASC')
            ->where('a.paket_id', $id)
            ->where('jenis_paket', 'UMROH')
            ->from('paket a')
            ->join('admin c', 'c.travel_agent_id = a.travel_agent_id')
            ->join('travel_agent b', 'b.travel_agent_id = c.travel_agent_id')
            ->join('group d', 'd.koordinator_id = c.admin_id')
            ->get()->row_array();

        $ress['fasilitas_details'] = $this->db->where('b.paket_id', $ress['paket_id'])
            ->select(['b.paket_id', 'b.fasilitas_id'])
            ->from('paket_fasilitas b')
            ->join('fasilitas a', 'a.fasilitas_id = b.fasilitas_id')
            ->join('paket c', 'c.paket_id = b.paket_id')
            ->get()->result_array();

        $ress['syarat_details'] = $this->db->where('b.paket_id', $ress['paket_id'])
            ->select(['b.ref_prasyarat_id', 'b.paket_id'])
            ->from('paket_prasyarat b')
            ->join('ref_prasyarat a', 'a.ref_prasyarat_id = b.ref_prasyarat_id')
            ->join('paket c', 'c.paket_id = b.paket_id')
            ->get()->result_array();

        $ress['kegiatan_details'] = $this->db->where('b.paket_id', $ress['paket_id'])
            ->select(['b.ref_kegiatan_id', 'b.paket_id', 'waktu_selesai', 'waktu_mulai', 'keterangan_kegiatan'])
            ->from('jadwal_paket b')
            ->join('ref_kegiatan a', 'a.ref_kegiatan_id = b.ref_kegiatan_id')
            ->join('paket c', 'c.paket_id = b.paket_id')
            ->get()->result_array();

        return $ress;
    }

    public function _loadAllFasilitasDetailCategory()
    {
        $ress = $this->db->select('*')
            ->from('fasilitas_detail_category')
            ->get()->result_array();

        return $ress;
    }

    public function _loadAllFasilitasByTravelAgentId($travel_agent_id)
    {
        $ress = $this->db->where('a.travel_agent_id', $travel_agent_id)
            ->select([
                'a.fasilitas_id', 'a.fasilitas_name', 'a.fasilitas_desc', 'a.fasilitas_star', 'a.fasilitas_motto', 'a.file_image',
                'c.fasilitas_category_name', 'c.fasilitas_category_desc'
            ])
            ->from('fasilitas a')
            ->join('travel_agent b', 'b.travel_agent_id = a.travel_agent_id')
            ->join('fasilitas_category c', 'c.fasilitas_category_id = a.fasilitas_category_id')
            ->get()->result_array();

        return $ress;
    }

    public function _loadGroupByPaket($paket_id) {
        $ress = $this->db->where('paket_id',$paket_id)
                ->order_by('group_nama ASC')
                ->get('group')->result_array();
                
        return $ress;        
    }

    public function _loadAllCategoryDetail()
    {
        $ress = $this->db->select('*')
            ->from('fasilitas_category_detail')
            ->get()->result_array();

        return $ress;
    }

    public function _loadAllFasilitasCategory()
    {
        $ress = $this->db->get('fasilitas_category')->result_array();

        return $ress;
    }
    
    public function _loadAllGroup() {
        $ress = $this->db->order_by('group_nama ASC')
                ->get('group')->result_array();
        return $ress;        
    }
    
    public function _getGroupJadwalById($paket_id) {
        $ress = $this->db->where('paket_id',$paket_id)
                ->get('group')->row_array();
        
        $ress['jadwal'] = $this->db->where('group_id',$ress['group_id'])
                            ->get('group_jadwal')
                            ->order_by('waktu_mulai ASC')
                            ->result_array();
        return $ress;
    }

    public function _loadContactByPaket($paket_id)
    {
        $ress = $this->db->where('a.paket_id', $paket_id)
            ->select(['b.nama', 'b.hotline', 'b.description', 'b.alamat', 'b.telp', 'b.telp2', 'b.email', 'c.nama_lengkap as ustad_pembina'])
            ->from('paket a')
            ->join('travel_agent b', 'b.travel_agent_id = a.travel_agent_id')
            ->join('admin c', 'c.admin_id = a.koordinator_id')
            ->get()->first_row();

        return $ress;
    }

    public function _loadFasilitasByCategoryId($id)
    {
        $ress = $this->db->select('*')
            ->where('a.fasilitas_category_id', $id)
            ->from('fasilitas a')
            ->join('fasilitas_category b', 'a.fasilitas_category_id = b.fasilitas_category_id')
            ->get()->result_array();


        $data = array();

        foreach ($ress as $row) {
           $row['details'] = $this->_loadFasilitasDetailByFasilitasId($row['fasilitas_id']);

            $data[] = $row;
        }

        return $data;
    }

    public function _loadFasilitasDetailByFasilitasId($fasilitas_id)
    {
        $ress = $this->db->where('a.fasilitas_id', $fasilitas_id)
            ->from('fasilitas_detail c')
            ->join('fasilitas a', 'a.fasilitas_id = c.fasilitas_id')
            ->join('fasilitas_detail_category b', 'c.fasilitas_detail_category_id = b.fasilitas_detail_category_id')
            ->get()->result_array();

        return $ress;
    }

    public function _loadAllJamaahByPaket($paket_id)
    {
        $ress = $this->db->where('a.paket_id', $paket_id)
            ->from('anggota_paket a')
            //->join('paket b', 'a.paket_id = b.paket_id')
            ->join('user c', 'a.user_id = c.user_id')
            ->get()->result_array();

        return $ress;
    }

    public function _loadAllReviewByPaket($paket_id)
    {
        $ress = $this->db->where('a.paket_id', $paket_id)
            ->from('review_paket b')
            ->join('paket a', 'a.paket_id = b.paket_id')
            ->join('user c', 'c.user_id = b.user_id')
            ->get()->result_array();

        return $ress;
    }

    public function _loadAllSyaratByPaket($paket_id) {
        $ress = $this->db->where('a.paket_id', $paket_id)
            ->from('paket_prasyarat b')
            ->join('ref_prasyarat c', 'c.ref_prasyarat_id = b.ref_prasyarat_id')
            ->join('paket a', 'a.paket_id = b.paket_id')
            ->get()->result_array();

        return $ress;
    }

    public function _loadAllJadwalByPaket($paket_id) {
        $ress = $this->db->where('a.paket_id', $paket_id)
            ->from('jadwal_paket b')
            ->join('paket a', 'a.paket_id = b.paket_id')
            ->join('ref_kegiatan c', 'c.ref_kegiatan_id = b.ref_kegiatan_id')
            ->get()->result_array();

        return $ress;
    }

    public function _loadAllKegiatan()
    {
        $ress = $this->db->get('ref_kegiatan')->result_array();

        return $ress;
    }

    public function _loadKoordinatorByTravelAgent($travel_agent_id)
    {
        $ress = $this->db->where('travel_agent_id', $travel_agent_id)
            ->from('admin')
            ->get()->result_array();

        return $ress;
    }

    public function _loadFasilitasById($fasilitas_id)
    {
        $ress = $this->db->where('fasilitas_id', $fasilitas_id)
            ->from('fasilitas')
            ->get()->row_array();

        $ress['details'] = $this->db->where('fasilitas_id', $ress['fasilitas_id'])
            ->select(['a.*'])
            ->from('fasilitas_detail a')
            ->join('fasilitas_detail_category b', 'b.fasilitas_detail_category_id = a.fasilitas_detail_category_id')
            ->get()->result_array();
        return $ress;
    }

    public function savePaketUmroh($data)
    {
        $this->Insert('paket', $data);
    }

    public function saveFasilitas($data)
    {
        $this->Insert('fasilitas', $data);
    }

    public function updateFasilitas($id ,$data)
    {
        $this->where('fasilitas_detail_id', $id);
        $this->update('fasilitas_detail', $data);
    }

    public function Insert($table,$data){
        $res = $this->db->insert($table, $data); // Kode ini digunakan untuk memasukan record baru kedalam sebuah tabel
        return $res; // Kode ini digunakan untuk mengembalikan hasil $res
    }

    public function _updatePaketById($id, $data)
    {
        $this->db->where('paket_id', $id);
        $this->db->update('paket', $data);
    }

    public function Delete($table, $where){
        $res = $this->db->delete($table, $where);

        return $res;
    }

    public function deletePaketUmrohById($id)
    {
        $this->db->where('paket_id', $id);
        $this->db->delete($this->table);
    }

}