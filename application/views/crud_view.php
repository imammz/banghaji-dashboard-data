<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php foreach ($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
        <?php endforeach; ?>
        
    </head>
    <body>

        <div style='height:20px;'></div> 
        <div class="btn-group btn-group-justified" role="group" aria-label="..." style="padding-left: 15px; padding-right: 15px;">
            <div class="btn-group" role="group">
                <a href="<?php echo site_url() ?>/Dt_doa/crud1" type="button" class="btn btn-success">Doa Umroh</a>
            </div>
            <div class="btn-group" role="group">
                <a href="<?php echo site_url() ?>/Dt_doa/crud3" type="button" class="btn btn-success">Doa Haji</a>
            </div>
            <div class="btn-group" role="group">
                <a href="<?php echo site_url() ?>/Dt_doa/crud2" type="button" class="btn btn-success">Doa Harian</a>
            </div>
        </div>
        <div>
            <?php echo $output; ?>
        </div>
        
        <?php foreach ($js_files as $file): ?>
            <script src="<?php echo $file; ?>"></script>
        <?php endforeach; ?>
    </body>
</html>
