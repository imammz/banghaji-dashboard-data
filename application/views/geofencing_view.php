<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/dataTables.bootstrap.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css">
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector:'textarea',
        statusbar : false
    });
</script>

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] <?php echo $title; ?>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">
                <?php echo $modul->description; ?>
            </div>
        </div>
        <div class="portlet-body">
            <ul class="nav nav-pills">
                <?php
                if( isset($action) && $action == 'index_create_edit_geofencing') {
                    $active_create_edit_geofencing = 'active in';
                    $active_geofencing = '';
                } else {
                    $active_geofencing = 'active in';
                    $active_edit_geofencing = '';
                }
                ?>
                <li class="<?php echo $active_create_edit_geofencing ?>">
                    <a href="#form_utama" data-toggle="tab" aria-expanded="true"> Form Geofencing </a>
                </li>
                <li class="<?php echo $active_geofencing ?>">
                    <a href="#form_geofencing" data-toggle="tab" aria-expanded="true"> Geofencing </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade <?php echo $active_create_edit_geofencing ?>" id="form_utama">
                    <form action="<?php echo isset($geofencing) ? site_url('Geofencing/process_update/'.$geofencing['geofencing_id']) : site_url('Geofencing/process_tambah') ?>" class="horizontal-form" method="POST">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="col-sm-2 control-label">Untuk Group :</label>
                                    <div class="col-sm-10">
                                        <select name="group_id" id="" class="form-control" required>
                                            <option value="" disabled selected>-- Pilih Group --</option>
                                            <?php 
                                            foreach ($groups as $row_group) { ?>
                                                <option <?php echo isset($geofencing) && $geofencing['group_id'] == $row_group['group_id'] ? 'selected' : '' ?> value="<?php echo $row_group['group_id'] ?>"><?php echo $row_group['group_nama'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="col-sm-2 control-label">Nama Lokasi :</label>
                                    <div class="col-sm-10">
                                        <select name="nama_lokasi" id="select-lokasi" class="form-control">
                                            <option value="" disabled selected>-- Pilih Lokasi --</option>
                                            <?php
                                            foreach ($master_geofencing as $row) { ?>
                                                <option <?php echo isset($geofencing) && ($geofencing['nama_lokasi'] == $row['nama_lokasi']) &&
                                                    ($geofencing['latitude'] == $row['latitude']) && ($geofencing['longitude'] == $row['longitude'])
                                                    ? 'selected' : '' ?> value="<?php echo $row['nama_lokasi'] . '|' . $row['latitude'] .'|'. $row['longitude']?>"><?php echo $row['nama_lokasi'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="col-sm-2 control-label">Radius :</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="radius" id="radius" class="form-control" value="<?php echo isset($geofencing) ? $geofencing['radius'] : '' ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <input type="hidden" id="location-lat" name="latitude" value="<?php echo isset($geofencing) ? $geofencing['latitude'] : ''?>">
                            <input type="hidden" id="location-long" name="longitude" value="<?php echo isset($geofencing) ? $geofencing['longitude'] : ''?>">
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <div id="pick-location" style="width: 550px; height: 400px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="col-sm-2 control-label">Kategori :</label>
                                    <div class="col-sm-10">
                                        <select name="kategori" id="select-lokasi" class="form-control">
                                            <option value="" disabled selected>-- Pilih Lokasi --</option>
                                            <?php
                                            foreach ($kategori as $key_kategori => $value_kategori) { ?>
                                                <option <?php echo isset($geofencing) && $geofencing['kategori'] == $key_kategori ? 'selected' : '' ?> value="<?php echo $value_kategori;?>"><?php echo $value_kategori;?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="col-sm-2 control-label">Judul :</label>

                                    <div class="col-sm-10">
                                        <input name="title" class="form-control" value="<?php echo isset($geofencing) ? $title : ''?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="col-sm-2 control-label">Arab Text :</label>

                                    <div class="col-sm-10">
                                        <textarea name="text_arab" class="form-control"><?php echo isset($geofencing) ? $text_arab : ''?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="col-sm-2 control-label">Latin Text :</label>

                                    <div class="col-sm-10">
                                        <textarea name="text_latin" class="form-control"><?php echo isset($geofencing) ? $text_latin : ''?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="col-sm-2 control-label">Arti :</label>

                                    <div class="col-sm-10">
                                        <textarea name="arti" class="form-control"><?php echo isset($geofencing) ? $arti : ''?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="form-group">
                                <div class="col-lg-12 col-lg-offset-1">
                                    <button type="submit" class="btn btn-green">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade <?php echo $active_geofencing ?>" id="form_geofencing">
                    <table class="table table-hover" id="d-table">
                        <thead>
                        <tr>
                            <th>No. </th>
                            <th>Nama Group</th>
                            <th>Nama Lokasi</th>
                            <th>Latitude </th>
                            <th>Longitude </th>
                            <th>Aksi </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ($geofencing_data as $row_geofencing) {
                            ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $row_geofencing['group_nama'] ?></td>
                                <td><?php echo $row_geofencing['nama_lokasi'] ?></td>
                                <td><?php echo $row_geofencing['latitude'] ?></td>
                                <td><?php echo $row_geofencing['longitude'] ?></td>
                                <td>
                                    <a href="<?php echo site_url('Geofencing/edit_geofencing/' . $row_geofencing['geofencing_id']) ?>" class="btn btn-default btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                    <a href="<?php echo site_url('Geofencing/delete_geofencing/' . $row_geofencing['geofencing_id']) ?>" class="btn btn-default btn-xs"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>

                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyDnxvjCXqwk14HqYKtSjBrNa25rlHtYkPc&libraries=places'></script>
<script src="<?php echo base_url() ?>assets/vendors/jquery-location-picker/locationpicker.jquery.min.js"></script>

<script>
    $(function () {
        $('#d-table').DataTable();

        $('#select-lokasi').on('change', function() {
            var val = $(this).val();
            var arr = val.split('|');

            $('#pick-location').locationpicker({
                location: {
                    latitude: arr[1],
                    longitude: arr[2]
                }
            });

            $('#location-lat').val(arr[1]);
            $('#location-long').val(arr[2]);

        });

        $('#pick-location').locationpicker({
            radius: 300,
            inputBinding: {
                radiusInput: $('#radius'),
                latitudeInput: $('#location-lat'),
                longitudeInput: $('#location-long')
            }
        });
    });
</script>