<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url().VENDORS ?>/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css">
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'#keterangan' });</script>
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] <?php echo $title; ?>
        </div>
    </div>
</div>
<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">
                Form Tambah Paket
            </div>
        </div>
        <div class="portlet-body">
            <button class="btn btn-default" type="button" onclick="window.location.href='<?php echo site_url('paket/manajemen') ?>'"><i class="fa fa-undo"></i> Kembali</button>
            <br><br>
            <ul class="nav nav-pills">
                <?php
                if( isset($action) && $action == 'edit_fasilitas') {
                    $active_edit_fasilitas = 'active in';
                    $active_paket = '';
                } else {
                    $active_paket = 'active in';
                    $active_edit_fasilitas = '';
                }
                ?>
                <li class="<?php echo $active_paket; ?>">
                    <a href="#form_utama" data-toggle="tab" aria-expanded="true"> Paket </a>
                </li>
                <li class="<?php echo $active_edit_fasilitas; ?>">
                    <a href="#form_fasilitas" data-toggle="tab" aria-expanded="true"> Fasilitas </a>
                </li>
                <li>
                    <a href="#master_fasilitas" data-toggle="tab" aria-expanded="true"> Master Fasilitas </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade <?php echo $active_paket; ?>" id="form_utama">
                    <form action="<?php echo isset($paket) ? site_url('paket/process_update_paket/'.$paket['paket_id']) : site_url('paket/process_tambah_paket') ?>" class="horizontal-form" method="POST">
                        <input type="hidden" name="travel_agent_id" value="<?php echo isset($paket) ? $paket['travel_agent_id'] : $this->session->userdata('user_session')['travel_agent_id'] ?>">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Nama Paket</label>
                                        <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-archive"></i>
                                    </span>
                                            <input type="text" id="paket_name" name="paket_name" class="form-control" value="<?php echo isset($paket) ? $paket['paket_name'] : null ?>">
                                        </div>
                                        <span class="help-block"> Masukan Nama Judul Paket Perjalanan Umroh </span>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Jumlah Kuota</label>
                                        <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user-md"></i>
                                    </span>
                                            <input type="number" id="" name="max_kuota" min="0" class="form-control" value="<?php echo isset($paket) ? $paket['max_kuota'] : null; ?>">
                                        </div>
                                        <span class="help-block"> Masukan Jumlah Kuota maksimal. </span>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Minimal Kuota</label>
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user-md"></i>
                                        </span>
                                            <input type="number" id="" name="min_kuota" min="0" class="form-control" value="<?php echo isset($paket) ? $paket['min_kuota'] : null; ?>">
                                        </div>
                                        <span class="help-block"> Masukan Jumlah Kuota minimal. </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Tanggal Berangkat</label>
                                        <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                            <input type="text" id="tanggal_berangkat" name="tanggal_berangkat" class="form-control form-control-inline input-medium date" value="<?php echo isset($paket) ? $paket['tanggal_berangkat'] : null; ?>">
                                        </div>
                                        <span class="help-block"> Masukan Tanggal Keberangkatan </span>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Tanggal Kepulangan</label>
                                        <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                            <input type="text" id="tanggal_pulang" name="tanggal_pulang" class="form-control form-control-inline input-medium date" value="<?php echo isset($paket) ? $paket['tanggal_pulang'] : null; ?>">
                                        </div>
                                        <span class="help-block"> Masukan Tanggal Kepulangan. </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="keterangan">Keterangan</label>
                                        <textarea id="keterangan" name="keterangan" class="form-control"><?php echo isset($paket) ? $paket['keterangan'] : null; ?></textarea>
                                        <span class="help-block"> Masukan Informasi Keterangan Seputar Paket Perjalanan Umroh. </span>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="currency">Mata Uang</label>
                                        <select name="currency" id="currency" class="form-control">
                                            <option <?php if(isset($paket) && $paket['currency'] == 'IDR') { echo 'selected'; } ?> value="IDR">IDR</option>
                                            <option <?php if(isset($paket) && $paket['currency'] == 'USD') { echo 'selected'; } ?> value="USD">USD</option>
                                        </select>
                                    </div>
                                    <span class="help-block"> Pilih Mata uang yang akan digunakan. </span>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="harga">Harga Paket</label>
                                        <input type="number" name="harga_total_paket" id="harga" class="form-control" min="0" value="<?php echo isset($paket) ? $paket['harga_total_paket'] : null; ?>">
                                        <span class="help-block"> Masukan total harga paket. </span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="discount">Discount</label>
                                        <input type="number" name="discount" id="discount" class="form-control" min="0" value="<?php echo isset($paket) ? $paket['discount'] : null; ?>">
                                        <span class="help-block"> Masukan discount (optional). </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="portlet-body">
                                <div class="tabbable-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#fasilitas_hotel" data-toggle="tab" aria-expanded="true"> Fasilitas Hotel </a>
                                        </li>
                                        <li class="">
                                            <a href="#maskapai" data-toggle="tab" aria-expanded="false"> Fasilitas Maskapai </a>
                                        </li>
                                        <li class="">
                                            <a href="#koordinator" data-toggle="tab" aria-expanded="false"> Kordinator </a>
                                        </li>
                                        <li class="">
                                            <a href="#syarat_ketentuan" data-toggle="tab" aria-expanded="false"> Syarat & Ketentuan </a>
                                        </li>
                                        <li class="">
                                            <a href="#jadwal_kegiatan" data-toggle="tab" aria-expanded="false"> Jadwal Kegiatan </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="fasilitas_hotel">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Hotel</th>
                                                            <th>Keterangan </th>
                                                            <th>Choose </th>
                                                            <td>Detail </td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach($fasilitas_hotel as $row_fasilitas_hotel) { ?>
                                                            <?php

                                                            $fasilitas_detail_hotel = '';
                                                            foreach ($row_fasilitas_hotel['details'] as $detail) {
                                                                $fasilitas_detail_hotel .=  ' '. $detail['fasilitas_detail_category_name']. '. ' ?>

                                                                <?php
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td> <?php echo $row_fasilitas_hotel['fasilitas_name'] ?> </td>
                                                                <td> <?php echo substr($row_fasilitas_hotel['fasilitas_desc'],0, 120) . '...'  ?></td>
                                                                <td>
                                                                    <input type="checkbox" class="form-control"
                                                                           <?php
                                                                           $fasilitas_details = (isset($paket) && isset($paket['fasilitas_details'])) ? $paket['fasilitas_details'] : null;
                                                                           if($fasilitas_details) {
                                                                               foreach ($fasilitas_details as $row_details) {
                                                                                   if($row_details['fasilitas_id'] == $row_fasilitas_hotel['fasilitas_id']) {
                                                                                       echo 'checked';
                                                                                   }
                                                                               }
                                                                           }
                                                                           ?>
                                                                           name="fasilitas_hotel_id[]" value="<?php echo $row_fasilitas_hotel['fasilitas_id'] ?>">
                                                                </td>
                                                                <td> <button type="button" class="btn btn-green" data-toggle="modal" data-target="#modalHotel"
                                                                             data-nama-hotel="<?php echo $row_fasilitas_hotel['fasilitas_name'] ?>"
                                                                             data-keterangan="<?php echo $row_fasilitas_hotel['fasilitas_desc'] ?>"
                                                                             data-bintang="<?php echo $row_fasilitas_hotel['fasilitas_star'] ?>"
                                                                             data-list-fasilitas="<?php echo $fasilitas_detail_hotel ?>"
                                                                    >Detail</button>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="maskapai">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Maskapai</th>
                                                            <th>Keterangan </th>
                                                            <th>Choose </th>
                                                            <td>Detail</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach($fasilitas_maskapai as $row_fasilitas_maskapai) { ?>
                                                            <tr>
                                                                <?php
                                                                $fasilitas_detail_maskapai = '';
                                                                foreach ($row_fasilitas_maskapai['details'] as $detail) {
                                                                    $fasilitas_detail_maskapai .=  ' '. $detail['fasilitas_detail_category_name']. '. ' ?>

                                                                    <?php
                                                                }
                                                                ?>
                                                                <td> <?php echo $row_fasilitas_maskapai['fasilitas_name'] ?> </td>
                                                                <td> <?php echo substr($row_fasilitas_maskapai['fasilitas_desc'],0, 120) . '...'  ?></td>
                                                                <td>
                                                                    <input type="checkbox"
                                                                        <?php
                                                                        if($fasilitas_details) {
                                                                            foreach ($fasilitas_details as $row_details) {
                                                                                if($row_details['fasilitas_id'] == $row_fasilitas_maskapai['fasilitas_id']) {
                                                                                    echo 'checked';
                                                                                }
                                                                            }
                                                                        }
                                                                        ?>
                                                                           class="form-control" name="fasilitas_maskapai_id[]" value="<?php echo $row_fasilitas_maskapai['fasilitas_id'] ?>">
                                                                </td>
                                                                <td> <button type="button" class="btn btn-green" data-toggle="modal" data-target="#modalHotel"
                                                                             data-nama-fasilitas="<?php echo $row_fasilitas_maskapai['fasilitas_name'] ?>"
                                                                             data-keterangan="<?php echo $row_fasilitas_maskapai['fasilitas_desc'] ?>"
                                                                             data-bintang="<?php echo $row_fasilitas_maskapai['fasilitas_star'] ?>"
                                                                             data-list-fasilitas="<?php echo $fasilitas_detail_maskapai ?>"
                                                                    >Detail</button></td>
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="koordinator">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Kordinator : </label>
                                                        <select name="koordinator_id" class="form-control">
                                                            <option value="0" selected disabled> -- Pilih Kordinator  -- </option>
                                                            <?php
                                                            foreach ($koordinators as $row_koordinator) { ?>
                                                                <option
                                                                        <?php
                                                                        $koordinator = (isset($paket)) ? $paket['koordinator_id'] : null;
                                                                        if($koordinator) {
                                                                            if($koordinator == $row_koordinator['admin_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                        }
                                                                        ?>
                                                                        value="<?php echo $row_koordinator['admin_id']?>"><?php echo $row_koordinator['nama_lengkap']?></option>
                                                            <?php }
                                                            ?>
                                                        </select>
                                                        <br/>
                                                        <br/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="jadwal_kegiatan">
                                            <?php
                                            $i = 1;
                                            foreach($kegiatan as $row_kegiatan) { ?>
                                                <div class="row">
                                                    <input type="hidden" name="jadwal[<?php echo $i ?>][ref_kegiatan_id]" value="<?php echo $row_kegiatan['ref_kegiatan_id'] ?>">
                                                    <div class="col-md-1">
                                                        <div class="form-group">
                                                            <h3><?php echo $i ?></h3>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="control-label text-center">Choose</label>
                                                            <input type="checkbox"
                                                                   <?php
                                                                   $kegiatan_details = (isset($paket)) ? $paket['kegiatan_details'] : null;
                                                                   if($kegiatan_details) {
                                                                       foreach ($kegiatan_details as $row_kegiatan_details) {
                                                                           if($row_kegiatan_details['ref_kegiatan_id'] == $row_kegiatan['ref_kegiatan_id']) {
                                                                               echo 'checked';
                                                                           }
                                                                       }
                                                                   }
                                                                   ?>
                                                                   class="form-control" name="jadwal[<?php echo $i ?>][choosed]" value="1">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="control-label">Judul Kegiatan</label>
                                                            <div class="input-group">
                                                                <?php echo $row_kegiatan['kegiatan']?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label">Keterangan Kegiatan</label>
                                                            <div class="input-group">
                                                                <?php
                                                                $keterangan_kegiatan = '';
                                                                if($kegiatan_details) {
                                                                    foreach ($kegiatan_details as $row_kegiatan_details) {
                                                                        if($row_kegiatan_details['ref_kegiatan_id'] == $row_kegiatan['ref_kegiatan_id']) {
                                                                            $keterangan_kegiatan = $row_kegiatan_details['keterangan_kegiatan'];
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                                <textarea class="form-control" name="jadwal[<?php echo $i ?>][keterangan_kegiatan]" id="keterangan_kegiatan"><?php echo $keterangan_kegiatan ?></textarea>
                                                            </div>
                                                            <span class="help-block"> Masukan keterangan kegiatan </span>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $waktu_mulai = null;
                                                    if($kegiatan_details) {
                                                        foreach ($kegiatan_details as $row_kegiatan_details) {
                                                            if($row_kegiatan_details['ref_kegiatan_id'] == $row_kegiatan['ref_kegiatan_id']) {
                                                                $waktu_mulai = $row_kegiatan_details['waktu_mulai'];
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label">Waktu mulai</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </span>
                                                                <input type="text" id="waktu_mulai" name="jadwal[<?php echo $i ?>][waktu_mulai]" class="form-control datetimepicker" placeholder="<?php if($waktu_mulai) { echo $waktu_mulai; } else { echo date("Y-m-d H:i:s"); }  ?>">
                                                            </div>
                                                            <span class="help-block"> Masukan Waktu Mulai </span>
                                                        </div>
                                                    </div>

                                                    <?php
                                                    $waktu_selesai = null;
                                                    if($kegiatan_details) {
                                                        foreach ($kegiatan_details as $row_kegiatan_details) {
                                                            if($row_kegiatan_details['ref_kegiatan_id'] == $row_kegiatan['ref_kegiatan_id']) {
                                                                $waktu_selesai = $row_kegiatan_details['waktu_selesai'];
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    <!--/span-->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label">Waktu Selesai</label>
                                                            <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                                <input type="text" id="waktu_selesai" name="jadwal[<?php echo $i ?>][waktu_selesai]" class="form-control datetimepicker" placeholder="<?php if($waktu_selesai) { echo $waktu_selesai; } else { echo date("Y-m-d H:i:s"); }  ?>">
                                                            </div>
                                                            <span class="help-block"> Masukan Waktu Selesai. </span>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <hr/>
                                            <?php $i++; } ?>
                                        </div>
                                        <div class="tab-pane" id="syarat_ketentuan">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Prasyarat </th>
                                                            <td>Choose</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $no = 1;
                                                        foreach($syarat as $row_syarat) { ?>
                                                            <tr>
                                                                <td> <?php echo $no; ?> </td>
                                                                <td> <?php echo $row_syarat['prasyarat'] ?> </td>
                                                                <td><input type="checkbox"
                                                                           <?php
                                                                           $syarat_details = (isset($paket)) ? $paket['syarat_details'] : null;
                                                                           if($syarat_details) {
                                                                               foreach ($syarat_details as $row_syarat_details) {
                                                                                   if($row_syarat_details['ref_prasyarat_id'] == $row_syarat['ref_prasyarat_id']) {
                                                                                       echo 'checked';
                                                                                   }
                                                                               }
                                                                           }
                                                                           ?>
                                                                           class="form-control" name="ref_prasyarat_id[]" value="<?php echo $row_syarat['ref_prasyarat_id']?>"> </td>
                                                                <td> <button type="button" class="btn btn-green" data-toggle="modal" data-target="#modalPrasyarat"
                                                                             data-detail="<?php echo $row_syarat['deskripsi'] ?>"
                                                                    >Detail</button></td>
                                                            </tr>
                                                            <?php $no++; }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="daftar_jamaah">

                                        </div>
                                        <div class="tab-pane" id="dokumentasi">

                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-check"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade <?php echo $active_edit_fasilitas; ?>" id="form_fasilitas" >
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="<?php echo isset($fasilitas) ? site_url('paket/process_update_fasilitas/'.$fasilitas['fasilitas_id']) : site_url('paket/process_tambah_fasilitas'); ?>" class="form-horizontal" method="POST">
                                <input type="hidden" name="travel_agent_id" value="<?php echo isset($fasilitas) ? $fasilitas['travel_agent_id'] : $this->session->userdata('user_session')['travel_agent_id'] ?>">
                                <div class="col-lg-6">
                                    <h3>Fasilitas</h3>
                                    <div class="form-group">
                                        <label for="fasilitas_name" class="control-label">Nama Fasilitas</label>
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-archive"></i>
                                        </span>
                                            <input type="text" id="fasilitas_name" name="fasilitas_name" class="form-control" value="<?php echo isset($fasilitas) ? $fasilitas['fasilitas_name'] : null; ?>">
                                        </div>
                                        <span class="help-block"> Masukan Nama Fasilitas</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Deskripsi Fasilitas</label>
                                        <textarea id="fasilitas_name" name="fasilitas_desc" class="form-control" rows="10"><?php echo isset($fasilitas) ? $fasilitas['fasilitas_desc'] : null; ?></textarea>
                                        <span class="help-block"> Masukan Deskripsi </span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Rating / Stars</label>
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-star"></i>
                                        </span>
                                            <input type="text" id="" name="fasilitas_star" class="form-control" value="<?php echo isset($fasilitas) ? $fasilitas['fasilitas_star'] : null; ?>">
                                        </div>
                                        <span class="help-block"> Masukan Jumlah Rating</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Motto</label>
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-quote-right"></i>
                                        </span>
                                            <input type="text" id="fasilitas_name" name="fasilitas_motto" class="form-control" value="<?php echo isset($fasilitas) ? $fasilitas['fasilitas_motto'] : null; ?>">
                                        </div>
                                        <span class="help-block"> Masukan Motto </span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Category</label>
                                        <select name="fasilitas_category_id" id="" class="form-control">
                                            <option value="">- Pilih -</option>
                                            <?php
                                            foreach ($fasilitas_categories as $row_category) { ?>

                                                <option <?php if(isset($fasilitas) && $fasilitas['fasilitas_category_id'] == $row_category['fasilitas_category_id']) { echo 'selected'; } ?> value="<?php echo $row_category['fasilitas_category_id'] ?>"><?php echo $row_category['fasilitas_category_name'] ?></option>

                                            <?php } ?>
                                        </select>
                                        <span class="help-block"> Pilih Kategori </span>
                                    </div>
                                </div>
                                <div class="col-lg-6" style="padding-left: 20px">
                                    <h3>Fasilitas Detail</h3>
                                    <button type="button" class="btn btn-green" id="add-row-btn"><i class="fa fa-plus"></i> Tambah Row</button>
                                    <br>
                                    <?php if( isset($action )) {?>
                                        <div class="detail">
                                            <?php
                                            foreach ($fasilitas['details'] as $row_fasilitas_detail) {
                                                ?>
                                                <div class="form-group">
                                                    <label for="detail_kategori" class="control-label">
                                                        Kategori Detail
                                                    </label>
                                                    <select name="detail_category[][id]" id="detail_kategori" class="form-control">
                                                        <option value="">- Pilih -</option>
                                                        <?php
                                                        foreach ($fasilitas_detail_categories as $row_fasilitas_detail_categories) { ?>
                                                            <option <?php if($row_fasilitas_detail['fasilitas_detail_category_id'] == $row_fasilitas_detail_categories['fasilitas_detail_category_id']) { echo 'selected'; } ?> value="<?php echo $row_fasilitas_detail_categories['fasilitas_detail_category_id'] ?>"><?php echo $row_fasilitas_detail_categories['fasilitas_detail_category_name'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="control-label">
                                                        Kategori Deskripsi
                                                    </label>
                                                    <textarea name="detail_category[][description]" id="" class="form-control"><?php echo $row_fasilitas_detail['description'] ?></textarea>
                                                </div>
                                            <?php }
                                            ?>
                                        </div>
                                    <?php }
                                    ?>
                                    <div id="kategori-template" class="template">
                                        <div class="category_id"></div>
                                        <div class="category_description"></div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-green"><i class="fa fa-save"></i> Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="master_fasilitas">
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-hover d-table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Fasilitas</th>
                                        <th>Kategori</th>
                                        <th>Rating</th>
                                        <th>Motto</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach($master_fasilitas as $row_master_fasilitas) { ?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $row_master_fasilitas['fasilitas_name'] ?></td>
                                            <td><?php echo $row_master_fasilitas['fasilitas_category_name'] ?></td>
                                            <td><?php echo $row_master_fasilitas['fasilitas_star'] ?></td>
                                            <td><?php echo $row_master_fasilitas['fasilitas_motto'] ?></td>
                                            <td>
                                                <button type="button" class="btn btn-default btn-xs" onclick="window.location.href='<?php echo site_url('paket/edit_fasilitas/'. $row_master_fasilitas['fasilitas_id']); ?>'"><i class="fa fa-edit"></i>&nbsp;
                                                    Edit</button>
                                                <a href="<?php echo site_url('paket/delete_fasilitas/'. $row_master_fasilitas['fasilitas_id']); ?>" class="btn btn-default btn-xs" onclick="return confirm('Anda yakin ingin menghapus ?');"><i class="fa fa-trash"></i>&nbsp;
                                                    Delete</a>
                                            </td>
                                        </tr>
                                    <?php $no++; }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalHotel" aria-hidden="true" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Information Detail</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Nama Hotel :</label>
                            <label class="nama"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Rating :</label>
                            <label class="bintang"><i class="fa fa-star text-yellow fa-fw"></i></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Keterangan :</label>
                            <label class="keterangan"></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalPrasyarat" aria-hidden="true" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Information Detail</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Keterangan :</label>
                            <label class="detail"></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url().VENDORS ?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script>
    $(document).ready(function() {
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD hh:mm:ss'
        });

        $('.date').datetimepicker({
            format: 'YYYY-MM-DD'
        });

        $('.d-table').DataTable();

        $('#modalHotel').on('show.bs.modal', function (event) {
            $('.nama').text($(event.relatedTarget).data('nama-hotel'));
            $('.bintang').text($(event.relatedTarget).data('bintang'));
            $('.keterangan').text($(event.relatedTarget).data('keterangan'));
            $('.latitude').text($(event.relatedTarget).data('latitude'));
            $('.longitude').text($(event.relatedTarget).data('longitude'));
            $('.lokasi').text($(event.relatedTarget).data('lokasi'));
            $('.jenis-kelamin').text($(event.relatedTarget).data('jenis-kelamin'));
            $('.no-hp').text($(event.relatedTarget).data('no-hp'));
            $('.kloter').text($(event.relatedTarget).data('kloter'));
            $('.alamat').text($(event.relatedTarget).data('alamat'));
        });

        $('#modalPrasyarat').on('show.bs.modal', function (event) {
            $('.detail').text($(event.relatedTarget).data('detail'));
        });

        $('#add-row-btn').click(function() {
            addRow();
        });

        var itemCount = 1;

        function addRow() {
            var template = $('#kategori-template');
            var clone = template.clone().removeAttr('id').removeClass('template').addClass('item_'+itemCount);

            $('.delete_btn', clone).html(
                '<button type="button" class="btn btn-danger deleted"><i class="fa fa-times"></i></button>'
            );

            $('.category_id', clone).html('<div class="form-group">' +
                '<label for="detail_kategori" class="control-label">' +
                'Kategori Detail' +
                '</label>' +
                '<select name="detail_category['+ itemCount +'][id]" id="detail_kategori" class="form-control">' +
                '<option value="">- Pilih -</option>' +
                <?php
                foreach ($fasilitas_detail_categories as $row_fasilitas_detail_categories) { ?>
                '<option value="<?php echo $row_fasilitas_detail_categories['fasilitas_detail_category_id'] ?>"><?php echo $row_fasilitas_detail_categories['fasilitas_detail_category_name'] ?></option>' +
                <?php } ?>
                '</select>' +
                '</div>');

            $('.category_description', clone).html('<div class="form-group">' +
                '<label for="" class="control-label">' +
                'Kategori Deskripsi' +
                '</label>' +
                '<textarea name="detail_category['+ itemCount+'][description]" id="" class="form-control"></textarea>' +
                '</div>');

            template.after(clone);
            itemCount++;

            //$('.container-row-added').add('<div class="row-added"><i class="fa fa-minus"></i></button></div>').appendTo('.container-row-added');
            //$('.row-added').html(clone);

        }



    })
</script>