<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/dataTables.bootstrap.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] <?php echo $modul->title; ?>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">
                Pengajuan Visa
            </div>
        </div>
        <div class="portlet-body">
            <button class="btn btn-green" type="button" onclick="window.location.href='<?php echo site_url('jamaah/tambah_pengajuan_visa') ?>'"><i class="fa fa-plus"></i>  Tambah Data Pengajuan Visa</button>

            <hr>
            <table class="table table-hover d-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Jamaah</th>
                        <th>Passcode</th>
                        <th>Group</th>
                        <th>STATUS</th>
                    </tr>
                </thead>

                <tbody>
                <?php
                $no = 1;
                foreach($jamaah_proses as $row) { ?>
                    <tr>
                        <td> <?php echo $no ?> </td>
                        <td> <?php echo $row['nama_lengkap']?> </td>
                        <td> <?php echo $row['passcode']?> </td>
                        <td> <?php echo $row['group_nama']?> </td>
                        <td>
                            <?php
                            if($row['workflow_id']=='001') $color = 'text-blue';
                            if($row['workflow_id']=='002') $color = 'text-blue';
                            if($row['workflow_id']=='003') $color = 'text-primary';
                            if($row['workflow_id']=='004') $color = 'text-success';
                            if($row['workflow_id']=='005') $color = 'text-waning';
                            if($row['workflow_id']=='006') $color = 'text-danger';
                            ?>
                            <span class="<?php echo $color ?>"><strong><?php echo $row['workflow_desc'] ?></strong></span>

                        </td>
                    </tr>
                    <?php $no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $('select').select2();
    $('.d-table').DataTable();
</script>

