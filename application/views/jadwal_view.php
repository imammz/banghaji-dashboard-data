<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/dataTables.bootstrap.css">
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] <?php echo $modul->title; ?>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">
                <?php echo $modul->description; ?>
            </div>
        </div>
        <div class="portlet-body">
            <ul class="nav nav-pills">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab" aria-expanded="true"> Membuat Jadwal Kegiatan </a>
                </li>
                <li>
                    <a href="#tab_1_2" data-toggle="tab" aria-expanded="true"> Daftar Jadwal Kegiatan </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="tab_1_1">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <h3> Informasi Jadwal Kegiatan Setiap Paket Umroh </h3></div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="#" class="horizontal-form">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label class="control-label">Nama Paket</label>
                                                    <select type="text" id="paket_id" name="paket_id" class="form-control" >
                                                        <option value="0">-- Pilih Paket Umroh --</option>
                                                        <?php foreach ($paket as $row) { ?>
                                                        <option value="<?php echo $row['paket_id'] ?>"> <strong> <?php echo $row['paket_name'] ?> </strong> </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <span class="help-block"> Masukan Nama Judul Paket Perjalanan Umroh </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <div class="input-group">
                                                    <label class="control-label">Nama Group</label>
                                                    <select type="text" id="paket_id" name="paket_id" class="form-control" >
                                                        <option value="0">-- Pilih Group --</option>
                                                         <?php foreach ($group as $row) { ?>
                                                        <option value="<?php echo $row['group_id'] ?>"> <strong> <?php echo $row['group_nama'] ?> </strong> </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <span class="help-block"> Masukan Nama Group dari  Paket Perjalanan Umroh </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <?php for ($i = 1; $i <= 10; $i++) { ?>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <h3><?php echo $i ?></h3>

                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">Judul Kegiatan</label>
                                                <div class="input-group">

                                                    <input type="date" id="judul_kegiatan" name="judul_kegiatan[]" class="form-control" >
                                                </div>
                                                <span class="help-block"> Masukan Judul Kegiatan</span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Keterangan Kegiatan</label>
                                                <div class="input-group">

                                                    <textarea class="form-control" name="keterangan_kegiatan[]" id="keterangan_kegiatan"></textarea>
                                                </div>
                                                <span class="help-block"> Masukan keterangan kegiatan </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Waktu mulai</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <input type="date" id="waktu_mulai" name="waktu_mulai[]" class="form-control datepicker" placeholder="<?php echo date("Y-m-d") ?>">
                                                </div>
                                                <span class="help-block"> Masukan Waktu Mulai </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Waktu Selesai</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <input type="date" id="waktu_selesai" name="waktu_selesai[]" class="form-control datepicker" placeholder="<?php echo date("Y-m-d") ?>">
                                                </div>
                                                <span class="help-block"> Masukan Waktu Selesai. </span>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <hr/>
                                    <hr/>
                                    <?php } ?>
                                </div>
                                <div class="form-actions text-center">
                                    <hr/>
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-check"></i> Simpan
                                    </button>
                                </div>
                            </form>
                            <!-- END FORM-->
                            <div class="clearfix margin-bottom-20"> </div>
                            <div class="clearfix margin-bottom-20"> </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="tab-pane" id="tab_1_2">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <h3> Data Jadwal Kegiatan Setiap Paket Umroh </h3></div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                               
                                <table class="table table-hover"  id="tableEktp" class="display" width="100%">
                                    <tr class="bg-primary"  scope="row">
                                        <th>No</th>
                                        <th>Nama Paket</th>
                                        <th>Tanggal Keberangkatan - Pulang</th>
                                        <th>Kordinator</th>
                                        <th>Detail Jadwal</th>
                                    </tr>

                                    <?php 
                                    $no = 1;
                                    foreach($paket as $row) { ?>   
                                    
                                    <tr class="bg-overlay">
                                        <td> <?php echo $no ?> </td>
                                        <td> <?php echo $row['paket_name'] ?> </td>
                                        <td> <?php echo Tanggal::formatDate($row['tanggal_berangkat']).' - '.Tanggal::formatDate($row['tanggal_pulang']) ?> </td>
                                        <td> <?php echo $row['tanggal_berangkat'] ?>  </td>
                                        <td>
                                            <button class="btn btn-warning" data-toggle="modal" data-target="#basicModal" >EDIT</button>
                                            &nbsp;
                                         </td>
                                    </tr>
                                    
                                    <?php $no++;  } ?>

                                </table>

                            <!-- END FORM-->
                            <div class="clearfix margin-bottom-20"> </div>
                            <div class="clearfix margin-bottom-20"> </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="clearfix margin-bottom-20"> </div>

        </div>

    </div>

</div>

<script>


    $('.datepicker').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '-3d'
    });


</script>


