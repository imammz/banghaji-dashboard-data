<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] <?php echo $modul->title; ?>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">
                Form Pengajuan Visa
            </div>
        </div>
        <div class="portlet-body">

            <button type="button" class="btn btn-green" onclick="window.location.href='<?php echo site_url('jamaah/pengajuan_visa')?>'"><i class="fa fa-undo"></i> Kembali</button>

            <br><br>

            <form action="<?php echo site_url('jamaah/pengajuan_visa_proses') ?>" method="POST" class="horizontal-form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="jamaah">Passcode Jamaah : </label>
                            <div class="input-group">
                                <select class="form-control" id="jamaah" name="anggota_paket_id">
                                    <option value="0">-- Pilih Passcode Jamaah --</option>
                                    <?php foreach ($jamaah as $row) { ?>
                                        <option value="<?php echo $row['anggota_paket_id'].'#'.$row['passcode'] ?>"><?php echo $row['passcode'].' ( '.$row['nama_lengkap'].' )'; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Persyaratan Yang Dipenuhi</label>
                            <ul>
                                <?php foreach($syarat as $row) { ?>
                                    <li> <input type="checkbox" value="<?php echo $row['ref_prasyarat_id'] ?>" id="syarat-<?php echo $row['ref_prasyarat_id'] ?>"/>
                                            <label class="bold" for="syarat-<?php echo $row['ref_prasyarat_id'] ?>"> <b><?php echo $row['prasyarat'] ?></b> </label>
                                        <br/> <sub><?php echo $row['deskripsi'] ?></sub>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-green" type="submit">Proses Pengajuan Visa</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('select').select2();

    $('input[type="checkbox"]:not(".switch")').iCheck({
        checkboxClass: 'icheckbox_minimal-grey',
        increaseArea: '20%' // optional
    });
    $('input[type="radio"]:not(".switch")').iCheck({
        radioClass: 'iradio_minimal-grey',
        increaseArea: '20%' // optional
    });
</script>