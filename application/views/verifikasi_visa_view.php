<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/dataTables.bootstrap.css">

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] <?php echo $modul->title; ?>
        </div>
    </div>
</div>


<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">
                <?php echo $modul->description; ?>
            </div>
        </div>
        <div class="portlet-body">
            <ul class="nav nav-pills">
                <li class="active">
                    <a href="#dalamProses" data-toggle="tab" aria-expanded="true"> Dalam Proses </a>
                </li>
                <li >
                    <a href="#ditolak" data-toggle="tab" aria-expanded="true"> Visa Yang Ditolak </a>
                </li>
                <li >
                    <a href="#diterima" data-toggle="tab" aria-expanded="true"> Visa Yang Diterima </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="dalamProses">
                    <div class="caption">
                        <h3> Proses Verifikasi Visa Jamaah </h3>
                    </div>

                    <table class="table table-hover d-table" width="100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Jamaah</th>
                            <th>Passcode</th>
                            <th>Group</th>
                            <th>PROSES</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach($jamaah_proses as $row) { ?>

                            <tr>
                                <td> <?php echo $no ?> </td>
                                <td> <?php echo $row['nama_lengkap']?> </td>
                                <td> <?php echo $row['passcode']?> </td>
                                <td> <?php echo $row['group_nama']?> </td>
                                <td>
                                    <a href="<?php echo base_url() ?>jamaah/status_proses_tolak/<?php echo $row['anggota_paket_id']?>/<?php echo $row['passcode']?>"  class="btn btn-danger" onclick="return confirm('Anda Yakin Akan Proses Penolakan Visa ?')">
                                        Visa Ditolak
                                    </a>
                                    -
                                    <a href="<?php echo base_url() ?>jamaah/status_proses_pending/<?php echo $row['anggota_paket_id']?>/<?php echo $row['passcode']?>" class="btn btn-blue" onclick="return confirm('Anda Yakin Akan Proses ?')">
                                        Butuh Kelengkapan Data
                                    </a>
                                    -
                                    <a href="<?php echo base_url() ?>jamaah/status_proses_terima/<?php echo $row['anggota_paket_id']?>/<?php echo $row['passcode']?>" class="btn btn-success" onclick="return confirm('Anda Yakin Akan Proses ?')">
                                        Visa Diterima
                                    </a>

                                </td>
                            </tr>

                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>

                <div class="tab-pane fade" id="ditolak">
                    <h3> Visa Jamaah yang ditolak </h3>
                    <table class="table table-hover d-table" width="100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Jamaah</th>
                            <th>Passcode</th>
                            <th>Group</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach($jamaah_ditolak as $row) { ?>

                            <tr class="bg-overlay">
                                <td> <?php echo $no ?> </td>
                                <td> <?php echo $row['nama_lengkap']?> </td>
                                <td> <?php echo $row['passcode']?> </td>
                                <td> <?php echo $row['group_nama']?> </td>
                            </tr>

                            <?php $no++; } ?>
                        </tbody>
                    </table>

                </div>
                <div class="tab-pane fade" id="diterima">

                    <h3> Visa Jamaah yang diterima </h3>
                    <table class="table table-hover d-table" width="100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Jamaah</th>
                            <th>Passcode</th>
                            <th>Group</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach($jamaah_diterima as $row) { ?>

                            <tr class="bg-overlay">
                                <td> <?php echo $no ?> </td>
                                <td> <?php echo $row['nama_lengkap']?> </td>
                                <td> <?php echo $row['passcode']?> </td>
                                <td> <?php echo $row['group_nama']?> </td>
                            </tr>

                            <?php $no++; } ?>
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
</div>
<script>


    $('.datepicker').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '-3d'
    });

    $('.d-table').DataTable();

</script>


