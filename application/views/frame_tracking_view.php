<!DOCTYPE html>
<html lang="en">
  <!-- Mirrored from swlabs.co/madmin/code/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 07 Sep 2015 02:54:06 GMT -->
  <head>
    <title>
      <?php echo WEB_TITLE?>
    </title>
    <?php $this->load->view(TEMPLATE.'/nav/meta');?>
  </head>
  <body class=" ">
    <div>
	
      <div class="news-ticker bg-red">
        <div class="container">
         
          <a id="news-ticker-close" href="javascript:;">
            <i class="fa fa-times">
            </i>
          </a>
        </div>
      </div>
	  
      <!--BEGIN BACK TO TOP-->
      <a id="totop" href="#">
        <i class="fa fa-angle-up">
        </i>
      </a>
      <!--END BACK TO TOP-->
      <!--BEGIN TOPBAR-->
      <div id="header-topbar-option-demo" class="page-header-topbar">
        <nav id="topbar" role="navigation" style="margin-bottom: 0; z-index: 2;" class="navbar navbar-default navbar-static-top">
          <div class="navbar-header">
            <button type="button" data-toggle="collapse" data-target=".sidebar-collapse" class="navbar-toggle">
              <span class="sr-only">
                Toggle navigation
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
            </button>
            <a id="logo" href="#" class="navbar-brand">
              <span class="fa fa-rocket">
              </span>
              <span class="logo-text">
                <?php //echo TEXT_LOGO?><small>BANG HAJI</small>
              </span>
              <span style="display: none" class="logo-text-icon">
               
              </span>
            </a>
          </div>
          <div class="topbar-main">
			<?php $this->load->view(TEMPLATE.'/nav/topbar');?>
          </div>
        </nav>
      </div>
      <!--END TOPBAR-->
	  
      <div id="wrapper">
        <!--BEGIN SIDEBAR MENU-->
        <nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
         <?php $this->load->view(TEMPLATE.'/nav/sidebar');?>
        </nav>
        <!--END SIDEBAR MENU-->
		
        <!--BEGIN PAGE WRAPPER-->
        <div id="page-wrapper">
			
			<div id="contentPage">
			
			<?php //$this->load->view($view); ?>
                                
                            
                            
			<iframe width="100%" style="min-height: 790px" 
                                src="https://banghaji-cccf1.firebaseapp.com/">
                                </iframe> 
			</div>
          <!--BEGIN TITLE & BREADCRUMB PAGE-->
          
            <?php //echo $_breadcrumb;?>
          
          <!--END TITLE & BREADCRUMB PAGE-->
          <!--BEGIN CONTENT-->
        <?php //echo $_content?>
        
          <!--END CONTENT-->
        </div>
		
        <!--BEGIN FOOTER-->
        <div id="footer">
         <?php $this->load->view(TEMPLATE.'/nav/footer');?>
        </div>
        <!--END FOOTER-->
		
        <!--END PAGE WRAPPER-->
      </div>
    </div>
    
  </body>
  <!-- Mirrored from swlabs.co/madmin/code/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 07 Sep 2015 02:57:54 GMT -->
</html>