<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/dataTables.bootstrap.css">
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] <?php echo $modul->title; ?>
        </div>
    </div>
</div>
<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">
                <?php echo $modul->description; ?>

            </div>
        </div>
        <div class="portlet-body">
            <button class="btn btn-green" type="button" onclick="window.location.href='<?php echo site_url('jamaah/tambah_calon_jamaah')?>'"><i class="fa fa-plus"></i> Tambah Data Calon Jamaah</button>
            <br><br>
            <table class="table table-hover d-table">
                <thead>
                <tr>
                    <th>No. </th>
                    <th>Nama Jamaah</th>
                    <th>Passcode</th>
                    <th>Nama Paket</th>
                    <th>Group</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                foreach($anggota_paket as $row) { ?>
                    <tr>
                        <td> <?php echo $no ?> </td>
                        <td> <?php echo $row['jamaah_nama'] ?> </td>
                        <td class="bold text-primary" style="font-size: larger"> <?php echo $row['passcode'] ?> </td>
                        <td> <?php echo $row['paket_name'] ?>  </td>
                        <td> <?php echo $row['group_nama'].' ( '.$row['kordinator_nama'].' )' ?>  </td>
                        <td>
                            <a href="<?php echo base_url() ?>jamaah/jamaah_hapus/<?php echo $row['anggota_paket_id'] ?>" class="btn btn-green" onclick="return confirm('Anda Yakin Ingin Menghapusnya ?')"  ><i class="fa fa-trash"></i> HAPUS</a>&nbsp;
                        </td>
                    </tr>
                    <?php $no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('.d-table').DataTable();
    })
</script>


