
<a id="menu-toggle" href="#" class="hidden-xs">
    <i class="fa fa-bars">
    </i>
</a>
<ul class="nav navbar navbar-top-links navbar-right mbn">
    <li id="topbar-chat" class="hidden-xs">
        <marquee direction="left" width="650px" style="color:white">
            <a href="javascript:void(0)" class="btn-chat marquee-text">
                Bang Haji Dashboard | Cara Kini Ke Tanah Suci
            </a>
        </marquee>
        <!--<img src="http://www.kanomas.com/images/hotel/mukhtara-international.jpg" alt="" width="40"/>-->
    </li>
    <li class="dropdown topbar-user">
        <a data-hover="dropdown" href="#" class="dropdown-toggle">
            <!--<img src="<?php echo base_url().IMG?>/gallery/Logo_BH_1.png" alt="" class="img-responsive img-circle"/>-->
            &nbsp;
            <span class="hidden-xs">
                <?php echo $this->session->userdata('user_session')['nama_lengkap']?>
            </span>
            &nbsp;
            <span class="caret">
            </span>
        </a>
        <ul class="dropdown-menu dropdown-user pull-right">
            <li>
                <a href="#">
                    <i class="fa fa-user">
                    </i>
                    Profil
                </a>
            </li>
            <li>
                <a href="<?php echo site_url('home/logout')?>">
                    <i class="fa fa-key">
                    </i>
                    Log Out
                </a>
            </li>
        </ul>
    </li>
    <!--<li id="topbar-chat" class="hidden-xs">
        <a href="javascript:void(0)" class="btn-chat">
          <i class="fa fa-comments">
          </i>
          <span class="badge badge-info">
            3
          </span>
        </a>
        </li>-->
</ul>