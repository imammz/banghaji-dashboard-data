    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="Thu, 19 Nov 1900 08:52:00 GMT">
    <!--<link rel="shortcut icon" href="/icons/favicon.html">
    <link rel="apple-touch-icon" href="/icons/favicon-2.html">
    <link rel="apple-touch-icon" sizes="72x72" href="/icons/favicon-72x72.html">
    <link rel="apple-touch-icon" sizes="114x114" href="/icons/favicon-114x114.html">-->
    <!--Loading bootstrap css-->
    <link rel="stylesheet" href="<?php echo base_url().VENDORS;?>/bootstrap-datepicker/css/datepicker3.css">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().VENDORS;?>/jquery-ui-1.10.4.custom/css/ui-lightness/jquery-ui-1.10.4.custom.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().VENDORS;?>/font-awesome/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().VENDORS;?>/bootstrap/css/bootstrap.min.css">
    <!--LOADING STYLESHEET FOR PAGE-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().VENDORS;?>/intro.js/introjs.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().VENDORS;?>/calendar/zabuto_calendar.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().VENDORS;?>/sco.message/sco.message.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().VENDORS;?>/intro.js/introjs.css">
    <!--Loading style VENDORS-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().VENDORS;?>/animate.css/animate.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().VENDORS;?>/jquery-pace/pace.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().VENDORS;?>/iCheck/skins/all.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().VENDORS;?>/jquery-notific8/jquery.notific8.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().VENDORS;?>/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <!--Loading style-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().CSS;?>/themes/style1/orange-blue.css" class="default-style">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().CSS;?>/themes/style1/orange-blue.css" id="theme-change" class="style-change color-change">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().CSS;?>/style-responsive.css">

	<script src="<?php echo base_url().JS;?>/jquery-1.10.2.min.js">
    </script>
    <script src="<?php echo base_url().JS;?>/jquery-migrate-1.2.1.min.js">
    </script>
    <script src="<?php echo base_url().JS;?>/jquery-ui.js">
    </script>
    <!--loading bootstrap js-->
    <script src="<?php echo base_url().VENDORS;?>/bootstrap/js/bootstrap.min.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js">
    </script>
    <script src="<?php echo base_url().JS;?>/html5shiv.js">
    </script>
    <script src="<?php echo base_url().JS;?>/respond.min.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/metisMenu/jquery.metisMenu.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/slimScroll/jquery.slimscroll.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/jquery-cookie/jquery.cookie.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/iCheck/icheck.min.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/iCheck/custom.min.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/jquery-notific8/jquery.notific8.min.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/jquery-highcharts/highcharts.js">
    </script>
    <script src="<?php echo base_url().JS;?>/jquery.menu.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/jquery-pace/pace.min.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/holder/holder.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/responsive-tabs/responsive-tabs.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/jquery-news-ticker/jquery.newsTicker.min.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/moment/moment.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/bootstrap-datepicker/js/bootstrap-datepicker.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/bootstrap-daterangepicker/daterangepicker.js">
    </script>
    <!--CORE JAVASCRIPT-->
    <script src="<?php echo base_url().JS;?>/main.js">
    </script>
    <!--LOADING SCRIPTS FOR PAGE-->
    <script src="<?php echo base_url().VENDORS;?>/intro.js/intro.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/calendar/zabuto_calendar.min.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/sco.message/sco.message.js">
    </script>
    <script src="<?php echo base_url().VENDORS;?>/intro.js/intro.js">
    </script>
    <script src="<?php echo base_url().JS;?>/index.js">
    </script>
  <script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
  <script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>