<div class="sidebar-collapse menu-scroll">
    <ul id="side-menu" class="nav">
        <li class="user-panel">
            <div class="thumb">
                <img src="<?php echo base_url()?>/assets/images/gallery/Logo_BH_1.png" alt="" class="img-circle"/>
            </div>
            <div class="info">
                <p>
                    <?php echo $this->session->userdata('user_session')['nama_lengkap']?> <br>

                </p>
                <ul class="list-inline list-unstyled">
                    <li>
                        <?php echo $this->session->userdata('user_session')['email']?>
                    </li>
                </ul>
            </div>
            <div class="clearfix">
            </div>
        </li>
        <li class="active">
            <a href="<?php echo site_url('Home')?>">
                <i class="fa fa-tachometer fa-fw">
                    <div class="icon-bg bg-orange">
                    </div>
                </i>
                <span class="menu-title">
                Home
                </span>
            </a>
        </li>
        <li class="user-panel">
            <div class="info">
                <ul class="list-inline list-unstyled">
                    <li>
                        <strong> Management Paket Umroh </strong>
                    </li>
                </ul>
            </div>
            <div class="clearfix">
            </div>
        </li>
        <li>
            <a href="<?php echo site_url('paket/manajemen')?>">
                <i class="fa fa-archive">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Paket Umroh</span>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('paket/kalender')?>">
                <i class="fa fa-calendar">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Kalendar Umroh</span>
            </a>
        </li>
        <li class="user-panel">
            <div class="info">
                <ul class="list-inline list-unstyled">
                    <li>
                        <strong> Management Jamaah </strong>
                    </li>
                </ul>
            </div>
            <div class="clearfix">
            </div>
        </li>
        <li>
            <a href="<?php echo site_url('jamaah/daftar')?>">
                <i class="fa fa-user-plus">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Pendaftaran Calon Jamaah</span>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('jamaah/pengajuan_visa')?>">
                <i class="fa fa-file-text">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Pengajuan Visa</span>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('jamaah/verifikasi_visa')?>">
                <i class="fa fa-share">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Verifikasi Visa</span>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('jamaah/group')?>">
                <i class="fa fa-group">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Data Group</span>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('jamaah/tracking')?>">
                <i class="fa fa-map">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Tracking Jamaah</span>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('Geofencing')?>">
                <i class="fa fa-map">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Geofencing</span>
            </a>
        </li>
        <li class="user-panel">
            <div class="info">
                <ul class="list-inline list-unstyled">
                    <li>
                        <strong> Management Content Umroh </strong>
                    </li>
                </ul>
            </div>
            <div class="clearfix">
            </div>
        </li>
        <li>
            <a href="<?php echo site_url('Dt_doa')?>">
                <i class="fa fa-hand-paper-o">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Data Doa-doa</span>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('Dt_tausiyah')?>">
                <i class="fa fa-hand-paper-o">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Ceramah & Tausiyah</span>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('Artikel_blog')?>">
                <i class="fa fa-pencil-square">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Blogs & Artikel</span>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('Dt_video')?>">
                <i class="fa fa-file-audio-o">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Video & Audio</span>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('Dt_tips')?>">
                <i class="fa fa-quote-left">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Tips Haji & Umroh</span>
            </a>
        </li>
        <!--
        <li class="user-panel">
           <div class="info">

              <ul class="list-inline list-unstyled">
                <li>
                  <strong>  Travel Agent  </strong>
                </li>
              </ul>
            </div>
            <div class="clearfix">
            </div>
            </li>-->
        <!--                 <li>
            <a href="<?php echo site_url('Travel_agent')?>">
              <i class="fa fa-institution"><div class="icon-bg bg-orange"></div></i>
              <span class="menu-title">Data Travel Agent*</span>
            </a>
            </li>-->
        <li class="user-panel">
            <div class="info">
                <ul class="list-inline list-unstyled">
                    <li>
                        <strong>  Master Data </strong>
                    </li>
                </ul>
            </div>
            <div class="clearfix">
            </div>
        </li>
        <!--              <li>
            <a href="<?php echo site_url('Fasilitas_category')?>">
              <i class="fa fa-bed"><div class="icon-bg bg-orange"></div></i>
              <span class="menu-title">Data Kategori Fasilitas</span>
            </a>
            </li>-->
        <!-- <li>
            <a href="<?php echo site_url('Dt_paket')?>">
              <i class="fa fa-circle-o"><div class="icon-bg bg-orange"></div></i>
              <span class="menu-title">Fasilitas Paket Haji & Umroh</span>
            </a>
            </li>  -->
        <!--<li>
            <a href="<?php echo site_url('dt_jamaah')?>">
              <i class="fa fa-circle-o"><div class="icon-bg bg-orange"></div></i>
              <span class="menu-title">Data Jamaah</span>
            </a>
            </li>-->
        <!--	      <li>
            <a href="<?php echo site_url('Dt_content_cat')?>">
            <i class="fa fa-tags"><div class="icon-bg bg-orange"></div></i>
            <span class="menu-title">Data Kategori Konten</span>
            </a>
            </li>-->
        <!--  <li>
            <a href="<?php echo site_url('Content_umrah')?>">
              <i class="fa fa-circle-o"><div class="icon-bg bg-orange"></div></i>
              <span class="menu-title">Berita Haji & Umroh</span>
            </a>
            </li> -->
        <!--			   <li>
            <a href="<?php echo site_url('Dt_bank')?>">
              <i class="fa fa-database"><div class="icon-bg bg-orange"></div></i>
              <span class="menu-title">Data Bank & Jenis Transaksi</span>
            </a>
            </li>-->
        <!--<li>
            <a href="<?php echo site_url('dt_content_cat')?>">
              <i class="fa fa-circle-o"><div class="icon-bg bg-orange"></div></i>
              <span class="menu-title">Data Metode Pembayaran</span>
            </a>
            </li>-->
        <!--<li>
            <a href="<?php echo site_url('dt_logs')?>">
              <i class="fa fa-circle-o"><div class="icon-bg bg-orange"></div></i>
              <span class="menu-title">Data Logs</span>
            </a>
            </li>-->
        <li>
            <a href="<?php echo site_url('Dt_user')?>">
                <i class="fa fa-users">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">User Management</span>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('Group')?>">
                <i class="fa fa-users">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Group Management</span>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('Kordinator')?>">
                <i class="fa fa-users">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Anggota Group</span>
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('Master_geofencing')?>">
                <i class="fa fa-users">
                    <div class="icon-bg bg-orange"></div>
                </i>
                <span class="menu-title">Geofencing Management</span>
            </a>
        </li>
        <!--<li>
            <a href="#">
              <i class="fa fa-laptop fa-fw">
                <div class="icon-bg bg-pink">
                </div>
              </i>
              <span class="menu-title">
                Social Media
              </span>
              <span class="fa arrow">
              </span>
            </a>
            <ul class="nav nav-second-level">
              <li>
                <a href="frontend-one-page.html">
                  <i class="fa fa-rocket">
                  </i>
                  <span class="submenu-title">
                    Daftar Jamaah
                  </span>
                </a>
              </li>
            <li>
                <a href="frontend-one-page.html">
                  <i class="fa fa-rocket">
                  </i>
                  <span class="submenu-title">
                    Jadwal
                  </span>
                </a>
              </li>
            </ul>
            </li>-->
    </ul>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/customjs/getMenu.js')?>"></script>