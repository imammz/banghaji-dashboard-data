<html>
<head>
    <title>
        <?php echo WEB_TITLE?>
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="Thu, 19 Nov 1900 08:52:00 GMT">
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/icons/favicon.html');?>">
    <link rel="apple-touch-icon" href="<?php echo base_url('assets/img/icons/favicon-2.html');?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('assets/img/icons/favicon-72x72.html');?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('assets/img/icons/favicon-114x114.html');?>">
    <!--Loading bootstrap css-->

    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-ui-1.10.4.custom/css/ui-lightness/jquery-ui-1.10.4.custom.min.css');?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css');?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/vendors/bootstrap/css/bootstrap.min.css');?>">
    <!--LOADING STYLESHEET FOR PAGE-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/vendors/intro.js/introjs.css');?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/vendors/calendar/zabuto_calendar.min.css');?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/vendors/sco.message/sco.message.css');?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/vendors/intro.js/introjs.css');?>">

    <!--Loading style-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/themes/style1/orange-blue.css');?>" class="default-style">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/themes/style1/orange-blue.css');?>" id="theme-change" class="style-change color-change">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/style-responsive.css');?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">


    </script>
    <!--CORE JAVASCRIPT-->
    <script src="<?php echo base_url('assets/js/main.js');?>">
    </script>

    </script>
    <script src="<?php echo base_url('assets/js/index.js');?>">
    </script>

    <script src="<?php echo base_url('assets/jquery/jquery-2.1.4.min.js')?>"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
</head>

<body>
<div class="row">
    <div class="main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 logs">
        <img class="login-logo" style="text-align: center;" src="<?php echo base_url('assets/images/gallery/Logo_BH_1.png'); ?>" width="250px">
        <div class="box-login">
            <?php echo form_open('login/login_post'); ?>
                <fieldset>
                    <legend>
                        Halaman Login Travel Agent
                    </legend>
                    <p>
                        Silahkan mengisi username & password anda
                        <br>

                        <?php
                        if(isset($_SESSION['error'])){
                        ?>
                    <div class="alert alert-danger" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error !</strong> <?php echo $_SESSION['error']; ?>
                    </div>
                    <?php

                    }
                    ?>
                    </p>
                    <div class="form-group">
                                <span class="input-icon">
                                    <input type="email" class="form-control" name="email" placeholder="Email" >
                                    	 </span>
                    </div>
                    <div class="form-group">
                                <span class="input-icon">

                                    <input type="password" class="form-control password" name="password" placeholder="Password" >
                                    <!--<a class="forgot" href="#">
                                        Lupa password
                                    </a>-->
                                </span>
                    </div>

                    <div style="float:left; padding: 6px;">
                        <a class="forgot" href="#">
                            Lupa password
                        </a>
                    </div>

                    <button type="submit" class="btn btn-green pull-right">
                        Login <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </fieldset>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
</body>
</html>