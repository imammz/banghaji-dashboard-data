<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/fullcalendar/fullcalendar.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/dataTables.bootstrap.css">

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] <?php echo $title; ?>
        </div>
    </div>
</div>


<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">
                <?php echo $modul->description; ?>
            </div>
        </div>

        <div class="portlet-body">
            <div class="row">
                <div class="col-lg-12">
                    <div id="loading">Loading...</div>
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="detail" aria-hidden="true" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Information Detail</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Nama Paket :</label>
                            <label class="nama-paket"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Tanggal Berangkat :</label>
                            <label class="tanggal-berangkat"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Tanggal Pulang :</label>
                            <label class="tanggal-pulang"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Harga :</label>
                            <label class="harga"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Jumlah Kuota :</label>
                            <label class="max-kuota"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Min Kuota :</label>
                            <label class="min-kuota"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Keterangan :</label>
                            <label class="keterangan"></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="<?php echo base_url() ?>assets/vendors/fullcalendar/fullcalendar.min.js"></script>
<script>

    $(document).ready(function() {

        $('#calendar').fullCalendar({
            header: {
                left:   'title',
                center: '',
                right: 'month, prev, next'
            },
            navLinks: true, // can click day/week names to navigate views
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            eventColor: '#488c6c',
            eventTextColor: '#FFFFFF',
            eventSources: {
                url: '<?php echo base_url('paket/get_calendar_data') ?>',
                error: function () {
                    $('#loading').show();
                }
            },
            loading: function(bool) {
                $('#loading').toggle(bool);
            },
            eventClick: function(callEvent) {

                $('#detail').on('show.bs.modal', function () {

                    $('.nama-paket').text( callEvent.title );
                    $('.tanggal-berangkat').text( callEvent.start.format('DD MMMM YYYY'));
                    $('.tanggal-pulang').text( callEvent.end.format('DD MMMM YYYY'));
                    $('.harga').text( callEvent.harga_total_paket );
                    $('.max-kuota').text( callEvent.max_kuota );
                    $('.min-kuota').text( callEvent.min_kuota );
                    $('.keterangan').text( callEvent.keterangan );
                });

                $('#detail').modal('toggle');

            }
        });

    });

</script>


