<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url().VENDORS ?>/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css">
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'#keterangan' });</script>
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] <?php echo $title; ?>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">
                Edit Paket Umroh
            </div>
        </div>
        <div class="portlet-body">
            <button class="btn btn-default" type="button" onclick="window.location.href='<?php echo site_url('paket/manajemen') ?>'"><i class="fa fa-undo"></i> Kembali</button>
            <form action="#" class="horizontal-form">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Nama Paket</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-archive"></i>
                                    </span>
                                    <input type="text" id="paket_name" name="paket_name" class="form-control" value="<?php echo $paket_detail['paket_name'] ?>" disabled>
                                </div>
                                <span class="help-block"> Masukan Nama Judul Paket Perjalanan Umroh </span>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Jumlah Kuota</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user-md"></i>
                                    </span>
                                    <input type="number" id="" name="max_kuota" min="0" class="form-control" value="<?php echo $paket_detail['max_kuota'] ?>" disabled>
                                </div>
                                <span class="help-block"> Masukan Jumlah Kuota maksimal. </span>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Minimal Kuota</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user-md"></i>
                                    </span>
                                    <input type="number" id="" name="min_kuota" min="0" class="form-control" value="<?php echo $paket_detail['min_kuota'] ?>" disabled>
                                </div>
                                <span class="help-block"> Masukan Jumlah Kuota minimal. </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Tanggal Berangkat</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" id="tanggal_berangkat" name="tanggal_berangkat" class="form-control form-control-inline input-medium datetimepicker" placeholder="<?php echo $paket_detail['tanggal_berangkat'] ?>" disabled>
                                </div>
                                <span class="help-block"> Masukan Tanggal Keberangkatan </span>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Tanggal Kepulangan</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" id="tanggal_pulang" name="tanggal_pulang" class="form-control form-control-inline input-medium datetimepicker" placeholder="<?php echo $paket_detail['tanggal_pulang'] ?>" disabled>
                                </div>
                                <span class="help-block"> Tanggal Kepulangan. </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="keterangan">Keterangan</label>
                                <label for="" class="form-control"><?php echo $paket_detail['keterangan'] ?></label>
                            </div>
                        </div>
                        <!--/span-->
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="currency">Mata Uang : </label>
                                <label class="form-control-static">
                                    <?php echo $paket_detail['currency'] ?>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="harga">Harga Paket</label>
                                <input type="number" name="harga_total_paket" id="harga" class="form-control" min="0" value="<?php echo $paket_detail['harga_total_paket'] ?>" disabled>
                                <span class="help-block"> Total harga paket. </span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="discount">Discount</label>
                                <input type="number" name="discount" id="discount" class="form-control" min="0" value="<?php echo $paket_detail['discount'] ?>" disabled>
                                <span class="help-block"> Discount. </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tabbable-custom">
                        <ul class="nav nav-tabs ul-edit">
                            <li class="active">
                                <a href="#fasilitas_hotel" data-toggle="tab" aria-expanded="true"> Fasilitas Hotel </a>
                            </li>
                            <li class="">
                                <a href="#maskapai" data-toggle="tab" aria-expanded="false"> Fasilitas Maskapai </a>
                            </li>
                            <li class="">
                                <a href="#syarat_ketentuan" data-toggle="tab" aria-expanded="false"> Syarat & Ketentuan </a>
                            </li>
                            <li class="">
                                <a href="#review" data-toggle="tab" aria-expanded="false"> Review </a>
                            </li>
                            <li class="">
                                <a href="#jadwal_kegiatan" data-toggle="tab" aria-expanded="false"> Jadwal Kegiatan </a>
                            </li>
                            <li class="">
                                <a href="#contact" data-toggle="tab" aria-expanded="false"> Contact </a>
                            </li>
                            <li class="">
                                <a href="#daftar_jamaah" data-toggle="tab" aria-expanded="false"> Daftar Jamaah </a>
                            </li>
                            <li class="">
                                <a href="#dokumentasi" data-toggle="tab" aria-expanded="false"> Dokumentasi </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="fasilitas_hotel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>Hotel</th>
                                                <th>Keterangan </th>
                                                <td>Detail</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($fasilitas_hotel as $row_fasilitas_hotel) { ?>
                                                <tr>
                                                    <td> <?php echo $row_fasilitas_hotel['fasilitas_name'] ?> </td>
                                                    <td> <?php echo substr($row_fasilitas_hotel['fasilitas_desc'],0, 120) . '...'  ?></td>
                                                    <td> <?php
                                                        $fasilitas_hotel_detail = '';
                                                        foreach ($row_fasilitas_hotel['details'] as $detail) {
                                                                $fasilitas_hotel_detail .=  ' '. $detail['fasilitas_detail_category_name']. '. ' ?>

                                                        <?php
                                                        }
                                                        ?></td>
                                                    <td> <button type="button" class="btn btn-green" data-toggle="modal" data-target="#modalHotel"
                                                                 data-nama-fasilitas="<?php echo $row_fasilitas_hotel['fasilitas_name'] ?>"
                                                                 data-keterangan="<?php echo $row_fasilitas_hotel['fasilitas_desc'] ?>"
                                                                 data-bintang="<?php echo $row_fasilitas_hotel['fasilitas_star'] ?>"
                                                                 data-list-fasilitas="<?php echo $fasilitas_hotel_detail ?>"
                                                        >Detail</button></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="maskapai">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>Maskapai</th>
                                                <th>Keterangan </th>
                                                <td>Detail</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($fasilitas_maskapai as $row_fasilitas_maskapai) { ?>
                                                <tr>
                                                    <?php
                                                    $fasilitas_hotel_detail = '';
                                                    foreach ($row_fasilitas_hotel['details'] as $detail) {
                                                        $fasilitas_hotel_detail .=  ' '. $detail['fasilitas_detail_category_name']. '. ' ?>

                                                        <?php
                                                    }
                                                    ?>
                                                    <td> <?php echo $row_fasilitas_maskapai['fasilitas_name'] ?> </td>
                                                    <td> <?php echo substr($row_fasilitas_maskapai['fasilitas_desc'],0, 120) . '...'  ?></td>
                                                    <td> <button type="button" class="btn btn-green" data-toggle="modal" data-target="#modalHotel"
                                                                 data-nama-fasilitas="<?php echo $row_fasilitas_maskapai['fasilitas_name'] ?>"
                                                                 data-keterangan="<?php echo $row_fasilitas_maskapai['fasilitas_desc'] ?>"
                                                                 data-bintang="<?php echo $row_fasilitas_maskapai['fasilitas_star'] ?>"
                                                                 data-list-fasilitas="<?php echo $fasilitas_hotel_detail ?>"
                                                        >Detail</button></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="syarat_ketentuan">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Syarat</th>
                                                <th>Deskripsi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $no_syarat = 1;
                                            foreach ($list_syarat as $row_syarat) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $no_syarat; ?></td>
                                                    <td><?php echo $row_syarat['prasyarat']; ?></td>
                                                    <td><?php echo $row_syarat['deskripsi']; ?></td>
                                                </tr>
                                                <?php $no_syarat++; } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="review">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover d-table">
                                            <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Travel Agent</th>
                                                <th>Rating</th>
                                                <th>Nama Jamaah</th>
                                                <th>Review</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $no = 1;
                                            foreach ($list_review as $row_review) {?>
                                                <tr>
                                                    <td><?php echo $no;?></td>
                                                    <td><?php echo $row_review['nama_travel_agent']?></td>
                                                    <td><?php echo $row_review['rate']?></td>
                                                    <td><?php echo $row_review['nama_lengkap']?></td>
                                                    <td><?php echo $row_review['review_comment']?></td>
                                                </tr>
                                                <?php
                                                $no ++; }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="jadwal_kegiatan">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Nama Jadwal Kegiatan</th>
                                                    <th>Tanggal</th>
                                                    <th>Jam</th>
                                                    <th>Deskripsi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $no_jadwal = 1;
                                            foreach ($list_jadwal_kegiatan as $row_jadwal_kegiatan) {
                                            ?>
                                                <tr>
                                                    <td><?php echo $no_jadwal; ?></td>
                                                    <td><?php echo $row_jadwal_kegiatan['kegiatan']; ?></td>
                                                    <td><?php echo Tanggal::formatDate($row_jadwal_kegiatan['waktu_mulai']); ?></td>
                                                    <td><?php echo substr($row_jadwal_kegiatan['waktu_selesai'], 10,10); ?></td>
                                                    <td><?php echo $row_jadwal_kegiatan['keterangan_kegiatan']; ?></td>
                                                </tr>
                                            <?php $no_jadwal++; } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="contact">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <h3><?php echo $fasilitas_contact->nama?></h3>
                                            <h4>Deskripsi</h4>
                                            <p>
                                                <?php echo $fasilitas_contact->description?>
                                            </p>
                                            <h4>Alamat</h4>
                                            <p>
                                                <?php echo $fasilitas_contact->alamat?>
                                            </p>
                                            <h4>No Telp</h4>
                                            <p>
                                                <?php echo $fasilitas_contact->telp?><br>
                                                <?php echo $fasilitas_contact->telp2?>
                                            </p>
                                            <h4>Email </h4>
                                            <p>
                                                <?php echo $fasilitas_contact->email?>
                                            </p>
                                            <h4>Ustad Pembina </h4>
                                            <p>
                                                <?php echo $fasilitas_contact->ustad_pembina?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="daftar_jamaah">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover d-table">
                                            <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Jamaah</th>
                                                <th>Asal Jamaah</th>
                                                <th>No Hp</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $no_jamaah = 1;
                                            foreach ($list_jamaah as $row_jamaah) {?>
                                                <tr>
                                                    <td><?php echo $no_jamaah;?></td>
                                                    <td><?php echo $row_jamaah['nama_lengkap']?></td>
                                                    <td><?php echo $row_jamaah['alamat']?></td>
                                                    <td><?php echo $row_jamaah['no_hp']?></td>
                                                </tr>
                                            <?php
                                            $no_jamaah ++; }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="dokumentasi">

                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalHotel" aria-hidden="true" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Information Detail</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Nama Hotel :</label>
                            <label class="nama"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Rating :</label>
                            <label class="bintang"><i class="fa fa-star text-yellow fa-fw"></i></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Keterangan :</label>
                            <label class="keterangan"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">List Fasilitas :</label>
                            <label class="list-fasilitas"></label>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript" src="<?php echo base_url().VENDORS ?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script>
    $(document).ready(function() {
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss'
        });

        $('.d-table').DataTable();

        $('#modalHotel').on('show.bs.modal', function (event) {
            $('.nama').text($(event.relatedTarget).data('nama-fasilitas'));
            $('.bintang').text($(event.relatedTarget).data('bintang'));
            $('.keterangan').text($(event.relatedTarget).data('keterangan'));
            $('.latitude').text($(event.relatedTarget).data('latitude'));
            $('.longitude').text($(event.relatedTarget).data('longitude'));
            $('.lokasi').text($(event.relatedTarget).data('lokasi'));
            $('.jenis-kelamin').text($(event.relatedTarget).data('jenis-kelamin'));
            $('.no-hp').text($(event.relatedTarget).data('no-hp'));
            $('.kloter').text($(event.relatedTarget).data('kloter'));
            $('.alamat').text($(event.relatedTarget).data('alamat'));
            $('.list-fasilitas').text($(event.relatedTarget).data('list-fasilitas'));
        });
    })
</script>