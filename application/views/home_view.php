<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/dataTables.bootstrap.css">
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] <?php echo $title; ?>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">Home
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-lg-2">
                    <img src="<?php echo base_url();?>/assets/images/gallery/bismillah.png" alt="Foto Jamaah" class="img img-responsive" width="250">
                </div>
                <div class="col-lg-10">
                    <ul class="list-group list-group-full">
                        <li class="list-group-item"><i class="fa fa-home"></i>&nbsp; <?php echo $koordinator['nama'] ?></li>
                        <li class="list-group-item"><i class="fa fa-file-archive-o"></i>&nbsp; <?php echo $koordinator['alamat'] ?></li>
                        <li class="list-group-item"><i class="fa fa-phone"></i>&nbsp; <?php echo $koordinator['telp'] ?></li>
                        <li class="list-group-item"><i class="fa fa-user"></i>&nbsp; Penangung Jawab : <?php echo $koordinator['penanggung_jawab'] ?></li>
                    </ul>
                </div>
            </div>

            <ul id="myTab" class="nav nav-tabs ul-edit ul-">
                <li class="active"><a href="#notifikasi" data-toggle="tab"><i class="fa fa-bell"></i> Notifikasi Panic Button</a></li>
                <li class=""><a href="#aktifitas" data-toggle="tab"><i class="fa fa-tasks"></i> Aktifitas Jamaah</a></li>
                <li class=""><a href="#feedback" data-toggle="tab"><i class="fa fa-commenting"></i> Feedback</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade active in" id="notifikasi">
                    <table class="table table-hover d-table">
                        <thead>
                        <tr class="condensed">
                            <th scope="col">No. <span class="column-sorter"></span></th>
                            <th scope="col"><span class="column-sorter"></span></th>
                            <th scope="col">Koordinator<span class="column-sorter"></span></th>
                            <th scope="col">Group<span class="column-sorter"></span></th>
                            <th scope="col">Laporan Jamaah<span class="column-sorter"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1;
                        foreach ($panic as $row) { ?>
                            <td><?php echo $no; ?></td>
                            <td>
                                <span class="img-shadow">
                                   <img width="40" src="https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/11885195_10203347389565385_7627655072305956422_n.jpg?oh=c2ea1b23b8a78395c3692db4e8e20efe&oe=58DDEEF8" alt="" class="media-object thumb">
                                </span>
                            </td>
                            <td><?php echo $row['nama_lengkap']; ?></td>
                            <td><?php echo $row['group_nama']; ?></td>
                            <td>
                                <button class="btn btn-green" type="button" data-toggle="modal" data-target="#detail-<?php echo $row['group_id']; ?>">Detail</button>
                            </td>
                            <div class="modal fade" id="detail-<?php echo $row['group_id']; ?>" aria-hidden="true" role="dialog" tabindex="-1">
                                <div class="modal-dialog modal-center">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <h4 class="modal-title">Information Detail</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="nama" class="control-label text-left">Nama :</label>
                                                        <label for=""><?php echo $row['nama_lengkap']; ?></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nama" class="control-label text-left">Group Nama :</label>
                                                        <label for=""><?php echo $row['group_nama']; ?></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nama" class="control-label text-left">Group Nama :</label>
                                                        <label for=""><?php echo $row['email']; ?></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nama" class="control-label text-left">Group Nama :</label>
                                                        <label for=""><?php echo $row['alamat']; ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <?php
                                                foreach ($row['alert'] as $row_panic) { ?>
                                                    <div class="col-lg-4">
                                                        Nama : <b><?php echo $row_panic['nama_lengkap'] ?></b>
                                                        <br>
                                                        Tanggal : <?php echo Tanggal::formatDate($row_panic['created_at'])?> - <?php echo substr($row_panic['created_at'],10,10)?>
                                                        <br>
                                                        Lokasi : <?php echo $row_panic['lokasi'] ?><br>
                                                        <button type="button" data-toggle="modal" data-target="#detailPanic"
                                                                data-nama-lengkap="<?php echo $row_panic['nama_lengkap'] ?>"
                                                                data-email="<?php echo $row_panic['email'] ?>"
                                                                data-keterangan="<?php echo $row_panic['keterangan'] ?>"
                                                                data-latitude="<?php echo $row_panic['latitude'] ?>"
                                                                data-longitude="<?php echo $row_panic['longitude'] ?>"
                                                                data-lokasi="<?php echo $row_panic['lokasi'] ?>"
                                                                data-image-path="<?php echo $row_panic['image_path'] ?>"
                                                                data-no-hp="<?php echo $row_panic['no_hp'] ?>"
                                                                data-kloter="<?php echo $row_panic['group_nama'] ?>"
                                                                data-jenis-kelamin="<?php echo $row_panic['jenis_kelamin'] ?>"
                                                                data-alamat="<?php echo $row_panic['alamat'] ?>"
                                                                class="btn btn-sm btn-green"
                                                        >Detail</button>
                                                        <br><br>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php $no++; } ?>
                        </tbody>
                    </table>

                </div>
                <div class="tab-pane fade" id="aktifitas">
                    <table class="table table-hover d-table">
                        <thead>
                        <tr>
                            <th scope="col">No. <span class="column-sorter"></span></th>
                            <th scope="col">Nama Jamaah<span class="column-sorter"></span></th>
                            <th scope="col">Passcode<span class="column-sorter"></span></th>
                            <th scope="col">Aktifitas<span class="column-sorter"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1;
                        foreach ($aktifitas_jamaah as $row) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td>
                                    <a href="javascript:void(0)">
                                        <h6 class="media-heading"><?php echo $row['nama_lengkap'] ?></h6>
                                    </a>
                                    <div><?php echo $row['email'] ?></div>
                                </td>
                                <td><?php echo $row['passcode'] ?></td>
                                <td>
                                    <?php if($row['activity'] != null) { ?>
                                        <button type="button" class="btn btn-green" data-toggle="modal" data-target="#activity-<?php echo $row['user_id'] ?>">
                                            Detail
                                        </button>
                                    <?php } ?>
                                    <div class="modal fade" id="activity-<?php echo $row['user_id'] ?>" aria-hidden="true" role="dialog" tabindex="-1">
                                        <div class="modal-dialog modal-center">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <h4 class="modal-title">Information Detail</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <?php
                                                        foreach ($row['activity'] as $row_act) {

                                                            if($row_act['activity_type'] == 'LOGIN') {
                                                                $label = 'label-success';
                                                            } else {
                                                                $label = 'label-danger';
                                                            }
                                                            ?>
                                                            <div class="col-lg-3">
                                                                <br>
                                                                <span class="label <?php echo $label; ?>"><?php echo $row_act['activity_type'] ?></span>
                                                                <br>
                                                                <?php echo Tanggal::formatDateTime($row_act['created_at']) ?><br>
                                                                Waktu setempat
                                                                <br>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="feedback">
                    <table class="table table-hover d-table">
                        <thead>
                        <tr>
                            <th>No. </th>
                            <th>Nama Jamaah</th>
                            <th>Isi Feedback</th>
                            <th>Tanggal Feedback</th>
                            <th>Jam Feedback</th>
                            <th>Koordinator</th>
                            <th>Nama Paket</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $z = 1;
                        foreach ($feedback as $row_feedback) { ?>
                            <tr>
                                <td><?php echo $z ?></td>
                                <td><?php echo $row_feedback['nama_jamaah'] ?></td>
                                <td><?php echo $row_feedback['feedback'] ?></td>
                                <td><?php echo Tanggal::formatDate(substr($row_feedback['group_created_at'], 0, 10)) ?></td>
                                <td><?php echo substr($row_feedback['group_created_at'], 11, 10) ?></td>
                                <td><?php echo $row_feedback['nama_koordinator'] ?></td>
                                <td><?php echo $row_feedback['paket_name'] ?></td>
                            </tr>
                        <?php $z++; } ?>
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="detailPanic" aria-hidden="true" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Information Detail</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Nama :</label>
                            <label class="nama"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Email :</label>
                            <label class="email"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Keterangan :</label>
                            <label class="keterangan"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Lokasi :</label>
                            LAT : <label class="latitude"></label> - LONG : <label class="longitude"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Nama Lokasi :</label>
                            <label class="lokasi"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">No Hp :</label>
                            <label class="no-hp"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Jenis Kelamin :</label>
                            <label class="jenis-kelamin"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Kloter :</label>
                            <label class="kloter"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Daerah Asal :</label>
                            <label class="alamat"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <img src="<?php echo base_url()?>/assets/images/maps_madinah.png" width="590">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(function () {
        $('input').iCheck();

        $('.d-table').DataTable();

        $('#detailPanic').on('show.bs.modal', function (event) {
            $('.nama').text($(event.relatedTarget).data('nama-lengkap'));
            $('.email').text($(event.relatedTarget).data('email'));
            $('.keterangan').text($(event.relatedTarget).data('keterangan'));
            $('.latitude').text($(event.relatedTarget).data('latitude'));
            $('.longitude').text($(event.relatedTarget).data('longitude'));
            $('.lokasi').text($(event.relatedTarget).data('lokasi'));
            $('.jenis-kelamin').text($(event.relatedTarget).data('jenis-kelamin'));
            $('.no-hp').text($(event.relatedTarget).data('no-hp'));
            $('.kloter').text($(event.relatedTarget).data('kloter'));
            $('.alamat').text($(event.relatedTarget).data('alamat'));
        });
    })
</script>
