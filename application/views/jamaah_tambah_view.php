<link rel="stylesheet" href="<?php echo base_url().VENDORS ?>/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css">

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] <?php echo $modul->title; ?>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">
                <?php echo $modul->description; ?>
            </div>
        </div>
        <div class="portlet-body">

            <button type="button" class="btn btn-green" onclick="window.location.href='<?php echo site_url('jamaah')?>'"><i class="fa fa-undo"></i> Kembali</button>

            <br><br>

            <form action="<?php echo site_url('jamaah/daftar_proses') ?>" method="post" class="horizontal-form" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group">
                                <label class="control-label">Untuk Paket Umroh</label> :
                                <select name="paket_id" id="" class="form-control" required>
                                    <option value="0"> - Pilih Paket - </option>
                                    <?php foreach($paket as $row) { ?>
                                        <option value="<?php echo $row['paket_id'] ?>">
                                            <?php echo $row['paket_name'] ?> (<?php echo Tanggal::formatDate($row['tanggal_berangkat']).' - '.Tanggal::formatDate($row['tanggal_pulang'])  ?>)
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Nama Lengkap</label>
                            <div class="input-group">
                                <input type="text" name="nama_lengkap" class="form-control" required >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <div class="input-group">
                                <input type="email" id="email" name="email" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="NIK">Nomor KTP</label>
                            <div class="input-group">
                                <input type="text" id="NIK" name="NIK" class="form-control" required >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Tanggal Lahir</label>
                            <div class="input-group">
                                <input type="text" id="tanggal_berangkat" name="tanggal_lahir" class="form-control form-control-inline input-medium datetimepicker" placeholder="<?php echo date('Y-m-d', strtotime('-19 years'))?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Jenis Kelamin</label>
                            <div class="radio mtx">
                                <label>
                                    <input type="radio" name="jenis_kelamin" value="L" checked="checked"/>&nbsp;
                                    PRIA
                                </label>
                            </div>
                            <div class="radio mtx">
                                <label>
                                    <input type="radio" name="jenis_kelamin" value="P"/>&nbsp;
                                    WANITA
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="alamat">Alamat Lengkap</label>
                            <div class="input-group">
                                <textarea id="alamat" name="alamat" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="no_hp">Nomor Handphone</label>
                            <div class="input-group">
                                <input type="text" name="no_hp" id="no_hp" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="nomor_pasport">Nomor Passport</label>
                            <div class="input-group">
                                <input type="text" id="nomor_pasport" name="nomor_pasport" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-green">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url().VENDORS ?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script>
    $(document).ready(function() {
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD'
        });

        $('input[type="checkbox"]:not(".switch")').iCheck({
            checkboxClass: 'icheckbox_minimal-grey',
            increaseArea: '20%' // optional
        });
        $('input[type="radio"]:not(".switch")').iCheck({
            radioClass: 'iradio_minimal-grey',
            increaseArea: '20%' // optional
        });
    })
</script>