<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
	<div class="page-header pull-left">
	  <div class="page-title">
		[ <i class="fa fa-info"></i> ] <?php echo $modul->title;?>
	  </div>
	</div>
</div>

<div class="page-content">
	<div class="portlet box">
		<div class="portlet-header">
			<div class="caption">
			  <?php echo $page_title?>
			</div>
		</div>
		
		<div class="portlet-body">
			<div class="panel-body pan">
				<form action="#" class="form-horizontal" id="form_doa">
					<div class="form-body pal">
						<div class="form-group">
						  <label for="judul" class="col-md-3 control-label">
							Judul Doa
							<span class='require'>
							  *
							</span>
						  </label>
						  <div class="col-md-9">
							<div class="input-icon right">
							  <input type="text" name="judul" id="judul" value="<?php if(isset($content)) echo $content->judul ?>" class="form-control"/>
							</div>
						  </div>
						</div>
						<div class="form-group">
						  <label for="content_arabic" class="col-md-3 control-label">
							Isi Doa (Arabic Text)
							<span class='require'>
							  *
							</span>
						  </label>
						  <div class="col-md-9">
							<div class="input-icon right">
							  <textarea class="form-control" cols="80" rows="5" name="content_arabic" id="content_arabic"> <?php if(isset($content)) echo $content->content_arabic ?> </textarea>
							</div>
						  </div>
						</div>
						<div class="form-group">
						  <label for="content" class="col-md-3 control-label">
							Terjemah
							<span class='require'>
							  *
							</span>
						  </label>
						  <div class="col-md-9">
							<div class="input-icon right">					  
							<textarea class="form-control" cols="80" rows="5" name="content"> <?php if(isset($content)) echo $content->content ?> </textarea>
							</div>
						  </div>
						</div>
						<!--<div class="form-group">
						  <label for="file_image" class="col-md-3 control-label">
							Gambar (Avatar)
							<span class='require'>
							  *
							</span>
						  </label>
						  <div class="col-md-9">
							<div class="input-icon right">					  
							  <input type="file" name="file_image"/>
							</div>
						  </div>
						</div>-->
					</div>
					<div class="form-actions">
						<div class="col-md-offset-3 col-md-9">
						  &nbsp;
						   <input type="hidden" name="id" id="id" value="<?php if(isset($content)) echo $content->content_umroh_id ?>" class="form-control"/>
							<a href="#" onclick="backlistdoa()" class="btn btn-danger">
							Batal
						  </a>
						  <button type="button" onclick="save()" class="btn btn-green">
							Proses
						  </button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/customjs/doa.js')?>"></script>