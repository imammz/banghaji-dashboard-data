<!DOCTYPE html>
<!-- Template Name: Clip-Two - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title>BANG HAJI</title>
		<!-- start: META -->
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: GOOGLE FONTS -->
<!--		<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
		 end: GOOGLE FONTS -->
                 <?php $this->load->view(TEMPLATE.'/nav/meta');?> 
		<!-- end: CLIP-TWO CSS -->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
	</head>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/tree/css/jquery.treetable.theme.default.css" />




<!-- end: HEAD -->
<body>
    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white" style="min-height: 1900px; padding-top: 50px;">

        <div class="row">

            <div class="col-md-12"> 
                <div class="row">
                    <div class="col-sm-2"> 
                        <a class="btn btn-danger btn-xs" href="http://localhost/renbut-sdmk/abk/index.php/entry/dinaskesehatanprov/data">
                            <i class="fa fa-refresh"></i>    Refresh Data
                        </a>
                    </div>
                    <div class="col-sm-4"> &nbsp; </div>

                    <div class="col-sm-6">
                        <form method="get" action="http://localhost/renbut-sdmk/abk/index.php/entry/dinaskesehatanprov/get/">

                            <select class="form-control" name="faskes_kode" onchange="this.form.submit()" required>
                                <option value="">-- Cari Group--</option>
                                    
                                    <option  value="51"> Group 1 - Prov. Bali </option>
                                                                </select>
                        </form>

                    </div>
                </div>
                <br/>
                                <table id="example-advanced">
                    <caption>
                        <a href="#" onclick="jQuery('#example-advanced').treetable('expandAll');
                                return false;">+ Buka Semua Data</a>
                        &nbsp; &nbsp;
                        <a href="#" onclick="jQuery('#example-advanced').treetable('collapseAll');
                                return false;">- Tutup Semua Data</a>
                    </caption>
                    <thead>
                        <tr>
                            <th>Paket</th>

                            <th width="100">Jumlah Jamaah</th>

                            <th width="120">AKSI</th>

                            
                        </tr>
                    </thead>
                    <tbody>

                           

                        <tr  data-tt-id='1'><td><span class='folder' style="font-size: 18px;"> <strong> <?php echo $this->session->userdata('user_session')['nama'] ?> </strong> </span></td><td>-</td><td>-</td></tr>

                        <tr  data-tt-id='1-1' data-tt-parent-id='1'><td><span class='folder' style="font-size: 14px;"> <strong> Paket Umroh Akhir Tahun </strong>  </span></td><td style="font-size: 14px;"> 7 </td><td> <button class="btn btn-dark-purple btn-xs" type="button" onclick="tambahBagian('51')"> <i class="fa fa-book"></i> Informasi Paket Umroh  </button> </td></tr>

                                                            <tr  data-tt-id='1-1-1' data-tt-parent-id='1-1'><td style="background-color: #99bcff;"><span class='folder' style="font-size: 14px;">  Group 1 (kordinator Ghiyast) </span>  </td><td style="font-size: 14px;"> 3 </td><td> <button class="btn btn-dark-green btn-xs" type="button" onclick="tambahSDMK('15058')"> <i class="fa fa-recycle"></i> Data Group </button> </td></tr>

                                                                <tr data-tt-id='1-1-1-1' data-tt-parent-id='1-1-1'><td style="font-size: 13px;"><span class='file'> <strong style="color: #00438a">Ahmad Ma'aruf</strong> </span></td><td style="font-size: 14px;"> - </td><td> <button class="btn btn-info btn-xs" type="button" onclick="detilABK('14096')"> <i class="fa fa-file-o"></i> Detil Form Jamaah</button> <a class="btn btn-success btn-xs" target="_blank" type="button" name="CETAK" href="http://localhost/renbut-sdmk/abk/index.php/entry/entry/detil_perhitungan_abk_excel/14096"><i class="fa fa-file-excel-o"></i> Cetak </a> </td></tr>
                                                                <tr data-tt-id='1-1-1-1' data-tt-parent-id='1-1-1'><td style="font-size: 13px;"><span class='file'> <strong style="color: #00438a">Raisa Andriana</strong> </span></td><td style="font-size: 14px;"> - </td><td> <button class="btn btn-info btn-xs" type="button" onclick="detilABK('14096')"> <i class="fa fa-file-o"></i> Detil Form Jamaah</button> <a class="btn btn-success btn-xs" target="_blank" type="button" name="CETAK" href="http://localhost/renbut-sdmk/abk/index.php/entry/entry/detil_perhitungan_abk_excel/14096"><i class="fa fa-file-excel-o"></i> Cetak </a> </td></tr>
                                                                <tr data-tt-id='1-1-1-1' data-tt-parent-id='1-1-1'><td style="font-size: 13px;"><span class='file'> <strong style="color: #00438a">Imam Mudzakkir</strong> </span></td><td style="font-size: 14px;"> - </td><td> <button class="btn btn-info btn-xs" type="button" onclick="detilABK('14096')"> <i class="fa fa-file-o"></i> Detil Form Jamaah</button> <a class="btn btn-success btn-xs" target="_blank" type="button" name="CETAK" href="http://localhost/renbut-sdmk/abk/index.php/entry/entry/detil_perhitungan_abk_excel/14096"><i class="fa fa-file-excel-o"></i> Cetak </a> </td></tr>

                                                                <tr  data-tt-id='1-1-2' data-tt-parent-id='1-1'><td style="background-color: #99bcff;"><span class='folder' style="font-size: 14px;">  Group 2 (kordinator Taufan Arfianto) </span>  </td><td style="font-size: 14px;"> 4 </td><td> <button class="btn btn-dark-green btn-xs" type="button" onclick="tambahSDMK('15058')"> <i class="fa fa-recycle"></i> Data Group </button> </td></tr>

                                                                <tr data-tt-id='1-1-2-1' data-tt-parent-id='1-1-2'><td style="font-size: 13px;"><span class='file'> <strong style="color: #00438a">Sagaaf Arsyad</strong> </span></td><td style="font-size: 14px;"> - </td><td> <button class="btn btn-info btn-xs" type="button" onclick="detilABK('14096')"> <i class="fa fa-file-o"></i> Detil Form Jamaah</button> <a class="btn btn-success btn-xs" target="_blank" type="button" name="CETAK" href="http://localhost/renbut-sdmk/abk/index.php/entry/entry/detil_perhitungan_abk_excel/14096"><i class="fa fa-file-excel-o"></i> Cetak </a> </td></tr>
                                                                <tr data-tt-id='1-1-2-2' data-tt-parent-id='1-1-2'><td style="font-size: 13px;"><span class='file'> <strong style="color: #00438a">Nabila Syakiep</strong> </span></td><td style="font-size: 14px;"> - </td><td> <button class="btn btn-info btn-xs" type="button" onclick="detilABK('14096')"> <i class="fa fa-file-o"></i> Detil Form Jamaah</button> <a class="btn btn-success btn-xs" target="_blank" type="button" name="CETAK" href="http://localhost/renbut-sdmk/abk/index.php/entry/entry/detil_perhitungan_abk_excel/14096"><i class="fa fa-file-excel-o"></i> Cetak </a> </td></tr>
                                                                <tr data-tt-id='1-1-2-3' data-tt-parent-id='1-1-2'><td style="font-size: 13px;"><span class='file'> <strong style="color: #00438a">Dinan</strong> </span></td><td style="font-size: 14px;"> - </td><td> <button class="btn btn-info btn-xs" type="button" onclick="detilABK('14096')"> <i class="fa fa-file-o"></i> Detil Form Jamaah</button> <a class="btn btn-success btn-xs" target="_blank" type="button" name="CETAK" href="http://localhost/renbut-sdmk/abk/index.php/entry/entry/detil_perhitungan_abk_excel/14096"><i class="fa fa-file-excel-o"></i> Cetak </a> </td></tr>
                                                                <tr data-tt-id='1-1-2-5' data-tt-parent-id='1-1-2'><td style="font-size: 13px;"><span class='file'> <strong style="color: #00438a">Maudy Ayunda</strong> </span></td><td style="font-size: 14px;"> - </td><td> <button class="btn btn-info btn-xs" type="button" onclick="detilABK('14096')"> <i class="fa fa-file-o"></i> Detil Form Jamaah</button> <a class="btn btn-success btn-xs" target="_blank" type="button" name="CETAK" href="http://localhost/renbut-sdmk/abk/index.php/entry/entry/detil_perhitungan_abk_excel/14096"><i class="fa fa-file-excel-o"></i> Cetak </a> </td></tr>
   
                                                                
                                       

                                    


                        </tbody>
                </table>



            </div>
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


<!-- start: MAIN JAVASCRIPTS -->
		<script src="http://localhost/renbut-sdmk/abk/vendor/jquery/jquery.min.js"></script>
		<script src="http://localhost/renbut-sdmk/abk/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="http://localhost/renbut-sdmk/abk/vendor/modernizr/modernizr.js"></script>
		<script src="http://localhost/renbut-sdmk/abk/vendor/jquery-cookie/jquery.cookie.js"></script>
		<script src="http://localhost/renbut-sdmk/abk/vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
		<script src="http://localhost/renbut-sdmk/abk/vendor/switchery/switchery.min.js"></script>
		
                <script src="http://localhost/renbut-sdmk/abk/vendor/maskedinput/jquery.maskedinput.min.js"></script>
		<script src="http://localhost/renbut-sdmk/abk/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
		<script src="http://localhost/renbut-sdmk/abk/vendor/autosize/autosize.min.js"></script>
		<script src="http://localhost/renbut-sdmk/abk/vendor/selectFx/classie.js"></script>
		<script src="http://localhost/renbut-sdmk/abk/vendor/selectFx/selectFx.js"></script>
                <script src="http://localhost/renbut-sdmk/abk/vendor/select2/select2.min.js"></script>
                <script src="http://localhost/renbut-sdmk/abk/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
		<script src="http://localhost/renbut-sdmk/abk/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
		<script src="http://localhost/renbut-sdmk/abk/vendor/DataTables/jquery.dataTables.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: CLIP-TWO JAVASCRIPTS -->
		<script src="http://localhost/renbut-sdmk/abk/assets/js/main.js"></script>
		<script src="http://localhost/renbut-sdmk/abk/assets/js/form-elements.js"></script>    <script src="http://localhost/renbut-sdmk/abk/assets/library/tree/jquery.treetable.js"></script>
    <script src="http://localhost/renbut-sdmk/abk/assets/library/gb/greybox.js"></script>


    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                                            jQuery(document).ready(function () {
                                                Main.init();
                                                jQuery('#example-advanced').treetable('expandAll');
                                                //jQuery('#example-advanced').treetable();
                                                $('.modal').modal({show: true});
                                                FormElements.init();

                                            });

    </script>

    <script>

        function tambahFaskes(NO_KAB) {
            GB_show("Tambah Data Faskes", 'http://localhost/renbut-sdmk/abk/index.php/entry/dinaskesehatanprov/tambahFaskes/' + NO_KAB, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }

        function tambahBagian(faskes_kode) {
            GB_show("Tambah Data Bagian/Bidang", 'http://localhost/renbut-sdmk/abk/index.php/entry/dinaskesehatanprov/tambahBagian/' + faskes_kode, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
		
		 function tambahSubBagian(faskes_kode) {
            GB_show("Tambah Data SubBidang / SubBagian", 'http://localhost/renbut-sdmk/abk/index.php/entry/dinaskesehatanprov/tambahSubBagian/' + faskes_kode, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }

        function editInstallasi(faskes_kode) {
            GB_show("Edit Data Bagian/Bidang - SubBidang/SubBagian", 'http://localhost/renbut-sdmk/abk/index.php/entry/dinaskesehatanprov/editInstallasi/' + faskes_kode, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }


        function hapusInstallasi(faskes_installasi_id) {
            GB_show("Hapus Installasi / Sub Installasi / Bagian / Sub bagian", 'http://localhost/renbut-sdmk/abk/index.php/entry/dinaskesehatanprov/hapusInstallasi/' + faskes_installasi_id, 300, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }

        function tambahSDMK(faskes_installasi_id) {
            GB_show("Hitung Data Kebutuhan SDMK", 'http://localhost/renbut-sdmk/abk/index.php/entry/dinaskesehatanprov/tambahSDMK/' + faskes_installasi_id, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
         function hapusSDMK(sdmk_faskes_installasi_id) {
            GB_show("Hapus SDMK", 'http://localhost/renbut-sdmk/abk/index.php/entry/dinaskesehatanprov/hapusSDMK/' + sdmk_faskes_installasi_id, 300, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function detilABK(sdmk_faskes_installasi_id) {
            GB_show("Detil Form JamaahData ABK", 'http://localhost/renbut-sdmk/abk/index.php/entry/entry/detil_perhitungan_abk/' + sdmk_faskes_installasi_id, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function editFaskes(faskes_kode) {
            GB_show("Edit Data Dinas Kesehatan Provinsi", 'http://localhost/renbut-sdmk/abk/index.php/entry/dinaskesehatanprov/editfaskes/' + faskes_kode, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function editInstallasi(installasi_id) {
            GB_show("Edit Data Bagian Bidang", 'http://localhost/renbut-sdmk/abk/index.php/entry/dinaskesehatanprov/editinstallasi/' + installasi_id, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function editSDMK(sdmk_faskes_installasi_id) {
            GB_show("Edit Data SDMK", 'http://localhost/renbut-sdmk/abk/index.php/entry/dinaskesehatanprov/editsdmk/' + sdmk_faskes_installasi_id, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
     


    </script>


    <script>


        $("#example-advanced").treetable({expandable: true});



// Highlight selected row
        $("#example-advanced tbody").on("mousedown", "tr", function () {
            $(".selected").not(this).removeClass("selected");
            $(this).toggleClass("selected");
        });


        $("#example-advanced .folder").each(function () {
            $(this).parents("#example-advanced tr").droppable({
                accept: ".file, .folder",
                drop: function (e, ui) {
                    var droppedEl = ui.draggable.parents("tr");
                    $("#example-advanced").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
                },
                hoverClass: "accept",
                over: function (e, ui) {
                    var droppedEl = ui.draggable.parents("tr");
                    if (this != droppedEl[0] && !$(this).is(".expanded")) {
                        $("#example-advanced").treetable("expandNode", $(this).data("ttId"));
                    }
                }
            });
        });
    </script>
    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>

