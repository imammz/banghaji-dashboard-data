<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/DataTables/media/css/dataTables.bootstrap.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css">

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] <?php echo $title; ?>
        </div>
    </div>
</div>


<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">
                <?php echo $modul->description; ?>
            </div>
        </div>
        <div class="portlet-body">
            <ul class="nav nav-pills">
                <?php
                if( isset($action) && $action == 'index_create_edit_master_geofencing') {
                    $active_edit_geofencing = 'active in';
                    $active_geofencing = '';
                } else {
                    $active_geofencing = 'active in';
                    $active_edit_geofencing = '';
                }
                ?>
                <li class="<?php echo $active_edit_geofencing ?>">
                    <a href="#form_utama" data-toggle="tab" aria-expanded="true"> Form Geofencing </a>
                </li>
                <li class="<?php echo $active_geofencing ?>">
                    <a href="#form_geofencing" data-toggle="tab" aria-expanded="true"> Master Geofencing </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade <?php echo $active_edit_geofencing ?>" id="form_utama">
                    <form action="<?php echo isset($geofencing) ? site_url('Master_geofencing/process_update/'.$geofencing['master_geofencing_id']) : site_url('Master_geofencing/process_tambah') ?>" class="horizontal-form" method="POST">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="col-sm-2 control-label">Nama Lokasi :</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="nama_lokasi" value="<?php echo isset($geofencing) ? $geofencing['nama_lokasi'] : ''?>" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <input type="hidden" id="location-lat" name="latitude" value="<?php echo isset($geofencing) ? $geofencing['latitude'] : ''?>">
                            <input type="hidden" id="location-long" name="longitude" value="<?php echo isset($geofencing) ? $geofencing['longitude'] : ''?>">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="col-sm-2 control-label">Cari lokasi :</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="search-lokasi">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <div id="pick-location" style="width: 550px; height: 400px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="form-group">
                                <div class="col-lg-12 col-lg-offset-1">
                                    <button type="submit" class="btn btn-green">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade <?php echo $active_geofencing ?>" id="form_geofencing">
                    <table class="table table-hover" id="d-table">
                        <thead>
                        <tr>
                            <th>No. </th>
                            <th>Nama Lokasi</th>
                            <th>Latitude </th>
                            <th>Longitude </th>
                            <th>Aksi </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ($geofencing_data as $row_geofencing) {
                        ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $row_geofencing['nama_lokasi'] ?></td>
                                <td><?php echo $row_geofencing['latitude'] ?></td>
                                <td><?php echo $row_geofencing['longitude'] ?></td>
                                <td>
                                    <a href="<?php echo site_url('Master_geofencing/edit_geofencing/' . $row_geofencing['master_geofencing_id']) ?>" class="btn btn-default btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                    <a href="<?php echo site_url('Master_geofencing/delete_geofencing/' . $row_geofencing['master_geofencing_id']) ?>" class="btn btn-default btn-xs"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>

                        <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyDnxvjCXqwk14HqYKtSjBrNa25rlHtYkPc&libraries=places'></script>
<script src="<?php echo base_url() ?>assets/vendors/jquery-location-picker/locationpicker.jquery.min.js"></script>

<script>
    $(function () {
        $('#d-table').DataTable();

        $('#pick-location').locationpicker({
            location: {
                latitude: <?php echo isset($geofencing) ? $geofencing['latitude'] : -6.1744651 ?>,
                longitude: <?php echo isset($geofencing) ? $geofencing['longitude'] : 106.82274499999994 ?>
            },
            enableAutocomplete: true,
            inputBinding: {
                latitudeInput: $('#location-lat'),
                longitudeInput: $('#location-long'),
                locationNameInput: $('#search-lokasi')
            }
        });
    });
</script>


