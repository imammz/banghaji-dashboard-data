<link rel="stylesheet" href="<?php echo base_url().VENDORS ?>/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css">
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'#keterangan' });</script>
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            [ <i class="fa fa-info"></i> ] <?php echo $title; ?>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="portlet box portlet-green">
        <div class="portlet-header">
            <div class="caption">
                Edit Paket Umroh
            </div>
        </div>
        <div class="portlet-body">
            <button class="btn btn-default" type="button" onclick="window.location.href='<?php echo site_url('paket/manajemen') ?>'"><i class="fa fa-undo"></i> Kembali</button>
            <form action="#" class="horizontal-form">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Nama Paket</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-archive"></i>
                                    </span>
                                    <input type="text" id="paket_name" name="paket_name" class="form-control" value="<?php echo $paket->paket_name ?>">
                                </div>
                                <span class="help-block"> Masukan Nama Judul Paket Perjalanan Umroh </span>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Jumlah Kuota</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user-md"></i>
                                    </span>
                                    <input type="number" id="" name="max_kuota" min="0" class="form-control">
                                </div>
                                <span class="help-block"> Masukan Jumlah Kuota maksimal. </span>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Minimal Kuota</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user-md"></i>
                                    </span>
                                    <input type="number" id="" name="min_kuota" min="0" class="form-control">
                                </div>
                                <span class="help-block"> Masukan Jumlah Kuota minimal. </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Tanggal Berangkat</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" id="tanggal_berangkat" name="tanggal_berangkat" class="form-control form-control-inline input-medium datetimepicker" placeholder="<?php echo date("Y-m-d H:i:s") ?>">
                                </div>
                                <span class="help-block"> Masukan Tanggal Keberangkatan </span>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Tanggal Kepulangan</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" id="tanggal_pulang" name="tanggal_pulang" class="form-control form-control-inline input-medium datetimepicker" placeholder="<?php echo date("Y-m-d H:i:s") ?>">
                                </div>
                                <span class="help-block"> Masukan Tanggal Kepulangan. </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="keterangan">Keterangan</label>
                                <textarea id="keterangan" name="keterangan" class="form-control"></textarea>
                                <span class="help-block"> Masukan Informasi Keterangan Seputar Paket Perjalanan Umroh. </span>
                            </div>
                        </div>
                        <!--/span-->
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="currency">Mata Uang</label>
                                <select name="currency" id="currency" class="form-control">
                                    <option value="IDR">IDR</option>
                                    <option value="USD">USD</option>
                                </select>
                            </div>
                            <span class="help-block"> Pilih Mata uang yang akan digunakan. </span>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="harga">Harga Paket</label>
                                <input type="number" name="harga_total_paket" id="harga" class="form-control" min="0">
                                <span class="help-block"> Masukan total harga paket. </span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="discount">Discount</label>
                                <input type="number" name="discount" id="discount" class="form-control" min="0">
                                <span class="help-block"> Masukan discount (optional). </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions text-center">
                    <div class="portlet-body">
                        <div class="tabbable-custom">
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a href="#fasilitas_hotel" data-toggle="tab" aria-expanded="true"> Fasilitas Hotel </a>
                                </li>
                                <li class="">
                                    <a href="#maskapai" data-toggle="tab" aria-expanded="false"> Fasilitas Maskapai </a>
                                </li>
                                <li class="">
                                    <a href="#koordinator" data-toggle="tab" aria-expanded="false"> Kordinator </a>
                                </li>
                                <li class="">
                                    <a href="#review" data-toggle="tab" aria-expanded="false"> Review </a>
                                </li>
                                <li class="">
                                    <a href="#jadwal_kegiatan" data-toggle="tab" aria-expanded="false"> Jadwal Kegiatan </a>
                                </li>
                                <li class="">
                                    <a href="#contact" data-toggle="tab" aria-expanded="false"> Contact </a>
                                </li>
                                <li class="">
                                    <a href="#daftar_jamaah" data-toggle="tab" aria-expanded="false"> Daftar Jamaah </a>
                                </li>
                                <li class="">
                                    <a href="#daftar_jamaah" data-toggle="tab" aria-expanded="false"> Dokumentasi </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="fasilitas_hotel">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Hotel</th>
                                                    <th>Keterangan </th>
                                                    <td>Detail</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach($fasilitas_hotel as $row_fasilitas_hotel) { ?>
                                                    <tr>
                                                        <td> <?php echo $row_fasilitas_hotel['fasilitas_name'] ?> </td>
                                                        <td> <?php echo substr($row_fasilitas_hotel['fasilitas_desc'],0, 120) . '...'  ?></td>
                                                        <td> <button type="button" class="btn btn-green" data-toggle="modal" data-target="#modalHotel"
                                                                     data-nama-hotel="<?php echo $row_fasilitas_hotel['fasilitas_name'] ?>"
                                                                     data-keterangan="<?php echo $row_fasilitas_hotel['fasilitas_desc'] ?>"
                                                                     data-bintang="<?php echo $row_fasilitas_hotel['fasilitas_star'] ?>"
                                                                     data-list-fasilitas="<?php echo $fasilitas_detail ?>"
                                                            >Detail</button></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="maskapai">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <label class="control-label col-sm-3"> Keberangkatan </label>
                                            <select id="paket_fasilitas" name="maskapai_fasilitas_id[]" class="col-sm-3" >
                                                <option value="0"> -- Pilih Maskapai  -- </option>
                                            </select>
                                            <label class="control-label col-sm-3"> Kepulangan </label>
                                            <select id="paket_fasilitas" name="maskapai_fasilitas_id[]" class="col-sm-3" >
                                                <option value="0"> -- Pilih Maskapai  -- </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br/> <hr/><hr/>
                                </div>
                                <div class="tab-pane" id="koordinator">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <label class="control-label col-sm-3">Nama Group : </label>
                                            <input type="text" name="group_nama" class="col-sm-3" id="group_nama"/>
                                            <br/>
                                            <br/>
                                        </div>
                                        <div class="row">
                                            <label class="control-label col-sm-3">Kordinator : </label>
                                            <select id="paket_fasilitas" name="kordinator_id" class="col-sm-4" >
                                                <option value="0"> -- Pilih Kordinator  -- </option>
                                            </select>
                                            <br/>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="jadwal_kegiatan">
                                    <form action="#" class="horizontal-form">
                                        <div class="form-body">
                                            <div class="row">
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Nama Group</label>
                                                        <div class="input-group">
                                                            <select type="text" id="paket_id" name="paket_id" class="form-control" >
                                                                <option value="0">-- Pilih Group --</option>
                                                                <?php foreach ($group as $row) { ?>
                                                                    <option value="<?php echo $row['group_id'] ?>"> <strong> <?php echo $row['group_nama'] ?> </strong> </option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <span class="help-block"> Masukan Nama Group dari  Paket Perjalanan Umroh </span>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <?php for ($i = 1; $i <= 10; $i++) { ?>
                                                <hr/>
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <div class="form-group">
                                                            <h3><?php echo $i ?></h3>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="control-label">Judul Kegiatan</label>
                                                            <div class="input-group">

                                                                <input type="date" id="judul_kegiatan" name="judul_kegiatan[]" class="form-control" >
                                                            </div>
                                                            <span class="help-block"> Masukan Judul Kegiatan</span>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label">Keterangan Kegiatan</label>
                                                            <div class="input-group">

                                                                <textarea class="form-control" name="keterangan_kegiatan[]" id="keterangan_kegiatan"></textarea>
                                                            </div>
                                                            <span class="help-block"> Masukan keterangan kegiatan </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label">Waktu mulai</label>
                                                            <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </span>
                                                                <input type="date" id="waktu_mulai" name="waktu_mulai[]" class="form-control datepicker" placeholder="<?php echo date("Y-m-d") ?>">
                                                            </div>
                                                            <span class="help-block"> Masukan Waktu Mulai </span>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label">Waktu Selesai</label>
                                                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                                <input type="date" id="waktu_selesai" name="waktu_selesai[]" class="form-control datepicker" placeholder="<?php echo date("Y-m-d") ?>">
                                                            </div>
                                                            <span class="help-block"> Masukan Waktu Selesai. </span>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <hr/>
                                                <hr/>
                                            <?php } ?>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-check"></i> Simpan
                        </button>
                    </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalHotel" aria-hidden="true" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Information Detail</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Nama Hotel :</label>
                            <label class="nama"></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Rating :</label>
                            <label class="bintang"><i class="fa fa-star text-yellow fa-fw"></i></label>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label input-sm text-left">Keterangan :</label>
                            <label class="keterangan"></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript" src="<?php echo base_url().VENDORS ?>/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script>
    $(document).ready(function() {
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss'
        });

        $('#modalHotel').on('show.bs.modal', function (event) {
            $('.nama').text($(event.relatedTarget).data('nama-hotel'));
            $('.bintang').text($(event.relatedTarget).data('bintang'));
            $('.keterangan').text($(event.relatedTarget).data('keterangan'));
            $('.latitude').text($(event.relatedTarget).data('latitude'));
            $('.longitude').text($(event.relatedTarget).data('longitude'));
            $('.lokasi').text($(event.relatedTarget).data('lokasi'));
            $('.jenis-kelamin').text($(event.relatedTarget).data('jenis-kelamin'));
            $('.no-hp').text($(event.relatedTarget).data('no-hp'));
            $('.kloter').text($(event.relatedTarget).data('kloter'));
            $('.alamat').text($(event.relatedTarget).data('alamat'));
        });
    })
</script>